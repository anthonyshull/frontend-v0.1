import { Document, Geoloc, UsersSm } from './common';
import { Program } from './program';
import { Space } from './space';

export interface Challenge {
  id: number;
  title: string;
  title_fr?: string;
  banner_url: string;
  banner_url_sm: string;
  logo_url: string;
  logo_url_sm: string;
  short_title: string;
  short_description: string;
  short_description_fr?: string;
  description: string;
  description_fr?: string;
  status: string;
  skills: string[];
  interests: number[];
  documents: Document[];
  geoloc: Partial<Geoloc>;
  country?: string;
  city?: string;
  address?: string;
  feed_id: number;
  users_sm: UsersSm[];
  program: Pick<Program, 'id' | 'short_title' | 'title' | 'title_fr' | 'custom_challenge_name'>;
  affiliated_spaces: Pick<
    Space,
    'banner_url' | 'banner_url_sm' | 'id' | 'short_description' | 'short_title' | 'status' | 'title'
  > | 'affiliation_status'[];
  launch_date?: Date;
  end_date?: Date;
  final_date?: Date;
  claps_count: number;
  follower_count: number;
  members_count: number;
  saves_count: number;
  needs_count: number;
  projects_count: number;
  created_at: Date;
  updated_at: Date;
  is_owner: boolean;
  is_admin: boolean;
  is_member: boolean;
  has_clapped: boolean;
  has_followed: boolean;
  has_saved: boolean;
  project_status: string;
}
