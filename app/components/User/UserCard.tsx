import Link from 'next/link';
import React, { FC, memo } from 'react';
import useTranslation from 'next-translate/useTranslation';
import H2 from '~/components/primitives/H2';
import { useModal } from '~/contexts/modalContext';
// import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';
import { DataSource } from '~/types';
import { TextWithPlural } from '~/utils/managePlurals';
import Box from '../Box';
import Card from '../Card';
import Chips from '../Chip/Chips';
import Title from '../primitives/Title';
import BtnFollow from '../Tools/BtnFollow';
import { ContactForm } from '../Tools/ContactForm';
import ReactTooltip from 'react-tooltip';
import ReactGA from 'react-ga';
import BasicChip from '../BasicChip';
// import Image from 'next/image';
import Image2 from '../Image2';
import { AvatarContainer, ContactButton, CardContainer } from './UserCard.styles';
import { theme } from 'twin.macro';

interface Props {
  id: number;
  firstName: string;
  lastName: string;
  nickName?: string;
  shortBio: string;
  skills?: string[];
  resources?: string[];
  status?: string;
  lastActive?: string;
  logoUrl: string;
  hasFollowed?: boolean;
  canContact?: boolean;
  source?: DataSource;
  avatarSize?: string;
  role?: string;
  projectsCount?: number;
  mutualCount?: number;
  noMobileBorder?: boolean;
}
const UserCard: FC<Props> = ({
  id,
  firstName = 'First name',
  lastName = 'Last name',
  nickName = '',
  shortBio = '_ _',
  skills = [],
  resources = [],
  status,
  logoUrl = '/images/default/default-user.png',
  hasFollowed,
  canContact,
  lastActive,
  avatarSize = '4rem',
  source,
  role,
  mutualCount,
  projectsCount,
  noMobileBorder = true,
}) => {
  const { userData } = useUserData();
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();

  const openMessageModal = () => {
    // capture the opening of the modal as a special modal page view to google analytics
    ReactGA.modalview('/send-message');
    showModal({
      children: <ContactForm itemId={id} closeModal={closeModal} />,
      title: t('user.contactModal.title', { userFullName: `${firstName} ${lastName}` }),
    });
  };

  const userUrl = `/user/${id}/${nickName}`;
  return (
    // noMobileBorder param will remove border and padding of card unless noMobileBorder is false
    <Card spaceY={3} noMobileBorder={noMobileBorder}>
      <CardContainer justifyContent="space-between">
        <Box row>
          <Link href={userUrl}>
            <a>
              <AvatarContainer mr={2} avatarSize={avatarSize}>
                {/* <div tw="animate-pulse bg-gray-200 w-full h-full rounded-full absolute" /> */}
                <Image2 src={logoUrl} priority />
              </AvatarContainer>
            </a>
          </Link>
          <Box justifyContent="center" px={2}>
            <Box row flexWrap="wrap" alignItems="center">
              <Link href={userUrl} passHref>
                <Title tw="pr-2">
                  <H2 fontSize={'4xl'} style={{ wordBreak: 'break-word' }}>
                    {firstName} {lastName}
                  </H2>
                </Title>
              </Link>
              {role && <BasicChip background="#ffc0cb">{t(`member.role.${role}`)}</BasicChip>}
              {canContact !== undefined && userData && userData.id !== id && canContact !== false && (
                <>
                  <ContactButton
                    onClick={openMessageModal}
                    onKeyUp={(e) =>
                      // execute only if it's the 'enter' key
                      (e.which === 13 || e.keyCode === 13) && openMessageModal()
                    }
                    tabIndex={0}
                    data-tip={t('user.contactModal.title', { userFullName: firstName + ' ' + lastName })}
                    data-for="contact"
                    // show/hide tooltip on element focus/blur
                    onFocus={(e) => ReactTooltip.show(e.target)}
                    onBlur={(e) => ReactTooltip.hide(e.target)}
                    size="29"
                    title="Contact user"
                  />
                  <ReactTooltip id="contact" effect="solid" place="bottom" />
                </>
              )}
            </Box>
            <div tw="line-clamp-2 my-1 break-all">{shortBio || '_ _'}</div>
            {/* <P mb={0} color={theme.colors.greys['500']}>
              {status}
            </P> */}
          </Box>
        </Box>
        <div tw="hidden sm:block mt-4 lg:mt-0">
          {(!userData || userData?.id !== id) && (hasFollowed !== undefined || source === 'algolia') && (
            <BtnFollow
              followState={hasFollowed}
              itemType="users"
              itemId={id}
              source={source}
              width={'6.25rem'}
              roundButton
              btnType="primary"
            />
          )}
        </div>
      </CardContainer>
      {/* {(skills.length !== 0 || resources.length !== 0) && skills.map((skill) => `${skill},`)} */}
      {(skills.length !== 0 || resources.length !== 0) && (
        <Box spaceY={2}>
          <Chips
            data={skills.map((skill) => ({
              title: skill,
              href: `/search/members/?refinementList[skills][0]=${skill}`,
            }))}
            overflowLink={`/user/${id}`}
            color={theme`colors.lightBlue`}
            showCount={3}
            smallChips
          />
          <Chips
            data={resources.map((resource) => ({
              title: resource,
              href: `/search/members/?refinementList[ressources][0]=${resource}`,
            }))}
            overflowLink={`/user/${id}`}
            color="#eff7ff"
            showCount={3}
            smallChips
          />
        </Box>
      )}
      {(mutualCount > 0 || projectsCount > 0) && (
        <Box color="#626E7E" row pt={1} flexWrap="wrap" justifyContent="space-between">
          {projectsCount ? (
            <Box pb={1} pr={6}>
              {`${t('user.info.currentlyOn')} ${projectsCount}`} <TextWithPlural type="project" count={projectsCount} />
            </Box>
          ) : (
            ''
          )}
          {/* show if viewed user is different than connected user */}
          {mutualCount > 0 && userData?.id !== id && (
            <Box>
              {mutualCount} <TextWithPlural type="mutualConnection" count={mutualCount} />
            </Box>
          )}
        </Box>
      )}
    </Card>
  );
};

export default memo(UserCard);
