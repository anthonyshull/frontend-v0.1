/* eslint-disable camelcase */
import Link from 'next/link';
import React from 'react';
import { useRouter } from 'next/router';
import { LayoutProps } from 'styled-system';
import Box from '~/components/Box';
import Card from '~/components/Card';
import H2 from '~/components/primitives/H2';
import Title from '~/components/primitives/Title';
import BtnSave from '~/components/Tools/BtnSave';
import { TextWithPlural } from '~/utils/managePlurals';
import styled from '~/utils/styled';

interface Props {
  id: number;
  title: string;
  title_fr?: string;
  short_title: string;
  short_description: string;
  short_description_fr?: string;
  clapsCount?: number;
  membersCount?: number;
  projectsCount?: number;
  needsCount?: number;
  has_saved: boolean;
  banner_url: string;
  width?: LayoutProps['width'];
  source?: 'algolia' | 'api';
  cardFormat?: string;
}
const SpaceCard = ({
  id,
  title,
  title_fr,
  short_title,
  short_description,
  short_description_fr,
  clapsCount,
  membersCount,
  projectsCount,
  needsCount,
  has_saved,
  banner_url = '/images/default/default-space.jpg',
  width,
  source,
  cardFormat,
}: Props) => {
  const router = useRouter();
  const { locale } = router;
  const spaceUrl = `/space/${short_title}`;
  const TitleFontSize = cardFormat !== 'compact' ? ['4xl', '5xl'] : '4xl';
  return (
    <Card imgUrl={banner_url} href={spaceUrl} width={width}>
      <Box row justifyContent="space-between" spaceX={4}>
        <Link href={spaceUrl} passHref>
          <Title tw="pr-2">
            <H2 fontSize={TitleFontSize}>{(locale === 'fr' && title_fr) || title}</H2>
          </Title>
        </Link>
        <BtnSave itemType="spaces" itemId={id} saveState={has_saved} source={source} />
      </Box>
      {/* <div style={{ color: "grey", paddingBottom: "5px" }}>
            <span> Last active today </span> <span> Prototyping </span>
          </div> */}
      <div tw="line-clamp-6 flex-1 color[#343a40]">
        {(locale === 'fr' && short_description_fr) || short_description}
      </div>
      {cardFormat !== 'compact' && (
        <Box row alignItems="center" justifyContent="space-between" spaceX={2}>
          <CardData value={membersCount} title={<TextWithPlural type="member" count={membersCount} />} />
          <CardData value={needsCount} title={<TextWithPlural type="need" count={needsCount} />} />
          <CardData value={projectsCount} title={<TextWithPlural type="project" count={projectsCount} />} />
          {/* <CardData value={clapsCount} title={<TextWithPlural type="clap" count={clapsCount}/>} /> */}
        </Box>
      )}
    </Card>
  );
};

const CardData = ({ value, title }) => (
  <Box justifyContent="center" alignItems="center">
    <div>{value}</div>
    <div>{title}</div>
  </Box>
);

export default SpaceCard;
