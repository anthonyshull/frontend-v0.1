import useTranslation from 'next-translate/useTranslation';
import useUserData from '~/hooks/useUserData';
import { Project } from '~/types';
import Box from '../Box';
import Grid from '../Grid';
import A from '../primitives/A';
import Button from '../primitives/Button';
import P from '../primitives/P';
import ProjectCard from '../Project/ProjectCard';
import Loading from '../Tools/Loading';
import NoResults from '../Tools/NoResults';
import SpinLoader from '../Tools/SpinLoader';
import useInfiniteLoading from '~/hooks/useInfiniteLoading';

// const OverflowGradient = styled.div`
//   ${layout};
//   width: 3rem;
//   height: 100%;
//   position: absolute;
//   right: 0;
//   /* background: linear-gradient(269.82deg, white 50.95%, rgba(241, 244, 248, 0) 134.37%); */
//   background: ${(p) =>
//     `linear-gradient(269.82deg, ${p.theme.colors.lightBlue} 50.95%, rgba(241, 244, 248, 0) 134.37%)`};
// `;

const SpaceProjects = ({ spaceId }) => {
  const projectsPerQuery = 24; // number of needs we get per query calls (make it 3 to test locally)
  // const [projectsEndpoints, setProjectsEndpoint] = useState(
  //   `/api/spaces/${spaceId}/projects?items=${projectsPerQuery}`
  // );
  // const { data: dataProjects, response } = useGet<{ projects: Project[] }>(projectsEndpoints);
  // const { data: dataPrograms, error: dataProgramsError } = useGet<{ programs }>(`/api/spaces/${spaceId}/programs`);
  // const [selectedProgramFilterId, setSelectedProgramFilterId] = useState(undefined);
  const { userData } = useUserData();
  const { t } = useTranslation('common');
  const { data: dataProjects, response, error, size, setSize } = useInfiniteLoading<{
    projects: Project[];
  }>((index) => `/api/spaces/${spaceId}/affiliates?affiliate_type=Project&page=${index + 1}`);

  const projects = dataProjects ? [].concat(...dataProjects?.map((d) => d.projects)) : [];
  const totalNumber = parseInt(response?.[0].headers['total-count']);
  const isLoadingInitialData = !dataProjects && !error;
  const isLoadingMore =
    isLoadingInitialData || (size > 0 && dataProjects && typeof dataProjects[size - 1] === 'undefined');
  const isEmpty = dataProjects?.[0]?.length === 0;
  const isReachingEnd = isEmpty || projects?.length === totalNumber;

  // const onFilterChange = (e) => {
  //   const id = e.target.name;
  //   const itemsNb = projects.length > projectsPerQuery ? projects.length : projectsPerQuery;
  //   if (id) {
  //     setSelectedProgramFilterId(Number(id));
  //     setProjectsEndpoint(`/api/programs/${id}/projects?items=${itemsNb}`);
  //   } else {
  //     setSelectedProgramFilterId(undefined);
  //     setProjectsEndpoint(`/api/spaces/${spaceId}/projects?items=${projectsPerQuery}`);
  //   }
  // };

  return (
    <div>
      <Box>
        <P>{t('general.attached_projects_explanation')}</P>
        {!userData && ( // if user is not connected
          <A href="/signin">
            {t('header.signIn')} {t('program.signinCta.project')}
          </A>
        )}
      </Box>
      <Box py={4} position="relative">
        {/* <Box position="relative">
          <OverflowGradient display={[undefined, undefined, 'none']} />
          <Filters
            resetButtonLabel="challenge.list_all"
            content={dataPrograms?.program
              // filter to hide draft programs
              ?.filter(({ status }) => status !== 'draft')
              .map(({ title, id }) => ({
                title,
                id,
              }))}
            onChange={(e) => onFilterChange(e)}
            isError={!!dataProgramsError}
            errorMessage="*Could not get programs filters"
            selectedId={selectedProgramFilterId}
          />
        </Box> */}
        {!dataProjects ? (
          <Loading />
        ) : projects?.length === 0 ? (
          <NoResults type="project" />
        ) : (
          <Grid gridGap={4} gridCols={[1, 2, undefined, 3]} display={['grid', 'inline-grid']} py={4}>
            {projects
              ?.filter(
                ({ challenges }) =>
                  // display only projects that have been accepted to the challenge (go through all project's challenges and return true if one of them has project_status === "accepted" )
                  // Object.values(challenges).some((challenge) => challenge.project_status === 'accepted')
                  true
              )
              .map((project, index) => (
                <ProjectCard
                  key={index}
                  id={project.id}
                  title={project.title}
                  shortTitle={project.short_title}
                  short_description={project.short_description}
                  members_count={project.members_count}
                  needs_count={project.needs_count}
                  followersCount={project.follower_count}
                  postsCount={project.posts_count}
                  has_saved={project.has_saved}
                  skills={project.skills}
                  banner_url={project.banner_url}
                />
              ))}
          </Grid>
        )}
        {
          // show load more button if object has more items than the default items we get from first call, or if we still have not attained last call page
          totalNumber > projectsPerQuery && size <= response?.[0].headers['total-pages'] && (
            <Box alignSelf="center" pt={4}>
              <Button onClick={() => setSize(size + 1)} disabled={isLoadingMore || isReachingEnd}>
                {isLoadingMore && <SpinLoader />}
                {isLoadingMore ? t('general.loading') : !isReachingEnd ? t('general.load') : t('general.noMoreResults')}
              </Button>
            </Box>
          )
        }
      </Box>
    </div>
  );
};

export default SpaceProjects;
