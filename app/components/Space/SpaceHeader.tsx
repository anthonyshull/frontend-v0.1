/* eslint-disable camelcase */
import { Edit } from '@emotion-icons/fa-solid/Edit';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { FC, useEffect, useRef, useState } from 'react';
import { display, flexbox, layout, space } from 'styled-system';
import Box from '~/components/Box';
import H1 from '~/components/primitives/H1';
import { useModal } from '~/contexts/modalContext';
import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';
import { Members, Space, Project } from '~/types';
import { TextWithPlural } from '~/utils/managePlurals';
import { renderTeam, useScrollHandler } from '~/utils/utils';
import A from '../primitives/A';
import Button from '../primitives/Button';
import { ProjectLinkToSpace } from '../Project/ProjectLinkToSpace';
import BtnJoin from '../Tools/BtnJoin';
import BtnSave from '../Tools/BtnSave';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import { styled } from 'twin.macro';
import BasicChip from '../BasicChip';
import BtnFollow from '../Tools/BtnFollow';

// import "./SpaceHeader.scss";

interface Props {
  space: Space;
  lang: string;
  members: Members;
}
const SpaceHeader: FC<Props> = ({ space, lang = 'en' }) => {
  const router = useRouter();
  const { userData } = useUserData();
  const [offSetTop, setOffSetTop] = useState();
  const headerRef = useRef();
  const isSticky = useScrollHandler(offSetTop);
  const { showModal, closeModal } = useModal();
  const { data: projectsData, mutate: mutateProjects } = useGet<{
    projects: Project[];
  }>(`/api/spaces/${space.id}/affiliates?affiliate_type=Project`);
  // const { data: dataExternalLink } = useGet<{ url: string; icon_url: string }[]>(`/api/spaces/${space?.id}/links`);
  const { t } = useTranslation('common');

  useEffect(() => {
    setOffSetTop(headerRef?.current?.offsetTop);
  }, [headerRef]);

  const {
    id,
    logo_url,
    members_count,
    needs_count,
    projects_count,
    title,
    title_fr,
    short_title,
    is_admin,
    is_member,
    is_owner,
    is_pending,
    has_followed,
    has_saved,
    status,
  } = space;

  const LogoImgComponent = () => (
    <Img
      src={logo_url || 'images/jogl-logo.png'}
      style={{ objectFit: 'contain', backgroundColor: 'white' }}
      mr={4}
      ml={[undefined, undefined, 4]}
      mb={[undefined, undefined, 2]}
      width={['5rem', '7rem', '9rem', '10rem']}
      height={['5rem', '7rem', '9rem', '10rem']}
      alignSelf="center"
      alt="space logo"
      mt={[0, undefined, '-90px', '-100px']}
    />
  );

  return (
    <Box width="100%" py={4}>
      <div tw="flex flex-col justify-between md:flex-row" ref={headerRef}>
        <div tw="flex">
          <div tw="flex-col hidden sm:flex">
            <LogoImgComponent />
          </div>
          <Box width="100%" justifyContent="space-between" flexDirection={['column', 'row', 'column', 'row']}>
            <Box pr={[0, undefined, 5]} pb={[3, undefined, 0]}>
              <div tw="flex flex-wrap justify-center sm:justify-start">
                <div tw="flex">
                  <div tw="block sm:hidden">
                    <LogoImgComponent />
                  </div>
                  <div tw="flex flex-wrap items-center">
                    <H1 fontSize={['2.18rem', '2.3rem', '2.5rem', '2.78rem']} pr={3}>
                      {(lang === 'fr' && title_fr) || title}
                    </H1>
                    {is_admin && (
                      <Link href={`/space/${short_title}/edit`} passHref>
                        <a tw="flex justify-start mr-4 mb-4 sm:(justify-center mb-0)">
                          <Edit size={18} title="Edit space" />
                          {t('entity.form.btnAdmin')}
                        </a>
                      </Link>
                    )}
                  </div>
                </div>
                <div tw="flex flex-wrap pb-2 items-center md:py-0">
                  {userData && <BtnSave itemType="spaces" itemId={id} saveState={has_saved} />}
                  <ShareBtns type="space" />
                  {/* Social medias */}
                  {/* {dataExternalLink && dataExternalLink?.length !== 0 && (
                    <div tw="flex flex-wrap gap-x-3 mx-6">
                      {[...dataExternalLink].map((link, i) => (
                        <a tw="items-center" key={i} href={link.url} target="_blank">
                          <img tw="w-10 hover:opacity-80" src={link.icon_url} />
                        </a>
                      ))}
                    </div>
                  )} */}
                  {/* display team members profile imgs */}
                  {/* {renderTeam(members?.slice(0, 6), 'space', space?.short_title, members_count, 'internal')} */}
                </div>
              </div>
              {space.space_type && <BasicChip background="lightgray">{t(`space.type.${space.space_type}`)}</BasicChip>}
            </Box>
          </Box>
        </div>

        <div tw="(flex flex-row justify-center) (pl-0 pt-5 space-x-2) md:(justify-start items-start pl-4 pt-0)">
          {is_member && <BtnFollow followState={has_followed} itemType="spaces" itemId={id} />}
          {!is_owner && (status !== 'completed' || (status === 'completed' && is_member)) && (
            // show join button only if we are not owner, or if status is different from "completed" (+ prevent user to join space if the space is set as completed)
            <BtnJoin itemType="spaces" itemId={id} joinState={is_pending ? 'pending' : is_member} isPrivate />
          )}
          {space.is_member && (
            <div tw="space-x-2">
              <Button
                onClick={() => {
                  showModal({
                    children: (
                      <ProjectLinkToSpace
                        alreadyPresentProjects={projectsData?.projects}
                        spaceId={space.id}
                        mutateProjects={mutateProjects}
                        closeModal={closeModal}
                      />
                    ),
                    title: t('attach.project.title'),
                    maxWidth: '50rem',
                  });
                }}
              >
                {t('attach.project.title')}
              </Button>
            </div>
          )}
        </div>
      </div>

      <div tw="flex self-center flex-wrap text-center text-violet underline space-x-5 mt-5 sm:(space-x-2.5 mt-3 self-start)">
        <A href={`/space/${router.query.short_title}?tab=members`} shallow noStyle scroll={false}>
          <div tw="hover:text-purple-900">
            {members_count + ' '}
            <TextWithPlural type="member" count={members_count} />
          </div>
        </A>
        <div tw="border-right[2px solid lightgrey]" />
        <A href={`/space/${router.query.short_title}?tab=projects`} shallow noStyle scroll={false}>
          <div tw="hover:text-purple-900">
            {projects_count + ' '}
            <TextWithPlural type="project" count={projects_count} />
          </div>
        </A>
        <div tw="border-right[2px solid lightgrey]" />
        <A href={`/space/${router.query.short_title}?tab=needs`} shallow noStyle scroll={false}>
          <div tw="hover:text-purple-900">
            {needs_count + ' '}
            <TextWithPlural type="need" count={needs_count} />
          </div>
        </A>
      </div>
      {/* heading with minimal infos, that sticks on top of screen, when you reached space logo */}
      <StickyHeading isSticky={isSticky} className="stickyHeading" bg="white" justifyContent="center">
        <Box row alignItems="center" px={[4, undefined, undefined, undefined, 0]}>
          <LogoImg src={space?.logo_url || 'images/jogl-logo.png'} mr={4} alt={`${space?.title} logo`} />
          <H1 fontSize={['1.8rem', '2.18rem']} lineHeight="26px">
            {(lang === 'fr' && space?.title_fr) || space?.title}
          </H1>
          <Box row pl={5} className="actions">
            {userData && (
              <>
                <Box pr={5}>
                  <BtnSave itemType="spaces" itemId={id} saveState={has_saved} />
                </Box>
              </>
            )}
            <ShareBtns type="space" />
          </Box>
        </Box>
      </StickyHeading>
    </Box>
  );
};

const Img = styled.img`
  ${[flexbox, space, display, layout]};
  object-fit: contain;
  border-radius: 50%;
  border: 3px solid white;
  box-shadow: 0 0px 7px black;
`;

const LogoImg = styled.img`
  ${[flexbox, space, layout]};
  object-fit: contain;
  border-radius: 50%;
  border: 3px solid white;
  box-shadow: 0 0px 7px black;
  width: 50px;
  height: 50px;
`;

const StickyHeading = styled(Box)`
  position: fixed;
  height: 70px;
  top: ${(p) => (p.isSticky ? '64px' : '-1000px')};
  width: 100%;
  z-index: 9;
  border-top: 1px solid grey;
  left: 0;
  transition: top 333ms;
  box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.15), 0 2px 3px rgba(0, 0, 0, 0.2);
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    height: 60px;
    .actions {
      display: none;
    }
    img {
      width: 40px;
      height: 40px;
    }
  }
  > div {
    max-width: 1280px;
    margin: 0 auto;
    width: 100%;
  }
`;
export default SpaceHeader;
