import React, { FC, ReactNode, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Alert from '~/components/Tools/Alert';
import { useApi } from '~/contexts/apiContext';
import { Challenge } from '~/types';
import A from '../primitives/A';
import Box from '../Box';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';

interface Props {
  challenge: Challenge;
  parentType: 'programs' | 'spaces';
  affiliationId?: number;
  callBack: () => void;
}
const ChallengeAdminCard: FC<Props> = ({ challenge, parentType, affiliationId, callBack }) => {
  const [sending, setSending] = useState<'remove' | 'accepted' | undefined>(undefined);
  const [error, setError] = useState<string | ReactNode>(undefined);
  const [imgToDisplay, setImgToDisplay] = useState('/images/default/default-challenge.jpg');
  const api = useApi();
  const { t } = useTranslation('common');

  // have different api route depending on parentType
  const route =
    parentType === 'programs'
      ? `/api/programs/${challenge.program.id}/challenges/${challenge.id}`
      : `/api/affiliations/${affiliationId}`;

  const onSuccess = () => {
    setSending(undefined);
    callBack();
  };

  const onError = (err) => {
    console.error(err);
    setSending(undefined);
    setError(t('err-'));
  };

  const acceptChallenge = () => {
    setSending('accepted');
    api
      .post(route, { status: 'accepted' })
      .then(() => onSuccess())
      .catch((err) => onError(err));
  };

  const removeChallenge = () => {
    setSending('remove');
    api
      .delete(route)
      .then(() => onSuccess())
      .catch((err) => onError(err));
  };

  const isChallengePending = false;
  // parentType === 'spaces'
  //   ? challenge.challenges.find((obj) => obj.challenge_id === parentId)?.project_status === 'pending'
  //   : // : project.spaces.find((obj) => obj.space_id === parentId)?.project_status === 'pending';
  //     false;

  useEffect(() => {
    challenge.logo_url && setImgToDisplay(challenge.logo_url);
  }, [challenge.logo_url]);

  const bgLogo = {
    backgroundImage: `url(${imgToDisplay})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    border: '1px solid #ced4da',
    borderRadius: '50%',
    height: '50px',
    width: '50px',
  };

  if (challenge) {
    return (
      <div>
        <Box flexDirection={['column', 'row']} justifyContent="space-between" key={challenge.id}>
          <Box row alignItems="center" pb={['3', '0']} spaceX={3}>
            <div style={{ width: '50px' }}>
              <div style={bgLogo} />
            </div>
            <Box>
              <A href={`/challenge/${challenge.short_title}`}>{challenge.title}</A>
              <Box fontSize="85%" pt={1}>
                {t('attach.status')}
                {t(`challenge.info.status_${challenge.status}`)}
              </Box>
            </Box>
          </Box>
          <Box row alignItems="center" justifyContent={['space-between', 'flex-end']}>
            <Box pr={8}>
              {t('attach.members')}
              {challenge.members_count}
            </Box>

            {isChallengePending ? ( // if challenge status is pending, display the accept/reject buttons
              <Box flexDirection={['column', 'row']}>
                <button
                  type="button"
                  className="btn btn-outline-success"
                  style={{ marginBottom: '5px', marginLeft: '8px' }}
                  disabled={sending === 'accepted'}
                  onClick={acceptChallenge}
                >
                  {sending === 'accepted' && <SpinLoader />}
                  {t('general.accept')}
                </button>
                <button
                  type="button"
                  className="btn btn-outline-danger"
                  style={{ marginBottom: '5px', marginLeft: '8px' }}
                  disabled={sending === 'remove'}
                  onClick={removeChallenge}
                >
                  {sending === 'remove' && <SpinLoader />}
                  {t('general.reject')}
                </button>
              </Box>
            ) : (
              <Button btnType="danger" disabled={sending === 'remove'} onClick={removeChallenge} tw="ml-3">
                {sending === 'remove' && <SpinLoader />}
                {t('general.remove')}
              </Button>
            )}
            {error && <Alert type="danger" message={error} />}
          </Box>
        </Box>
        <hr />
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default ChallengeAdminCard;
