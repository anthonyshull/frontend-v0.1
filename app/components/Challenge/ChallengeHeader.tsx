import { Edit } from '@emotion-icons/boxicons-solid/Edit';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { FC, Fragment, useContext } from 'react';
import useTranslation from 'next-translate/useTranslation';
import H1 from '~/components/primitives/H1';
import { useModal } from '~/contexts/modalContext';
import { UserContext } from '~/contexts/UserProvider';
import useGet from '~/hooks/useGet';
import useUserData from '~/hooks/useUserData';
import { Challenge, Project } from '~/types';
import { TextWithPlural } from '~/utils/managePlurals';
import styled from '~/utils/styled';
import Box from '../Box';
import Grid from '../Grid';
import A from '../primitives/A';
import Button from '../primitives/Button';
import { ProjectLinkModal } from '../Project/ProjectLinkModal';
import BtnFollow from '~/components/Tools/BtnFollow';
import BtnJoin from '../Tools/BtnJoin';
import BtnSave from '../Tools/BtnSave';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import { useTheme } from '~/utils/theme';
import { StatusCircle, statusToStep } from './ChallengeStatus';
import P from '../primitives/P';
// import "./ChallengeHeader.scss";

const StatusStep = styled(Box)`
  @media (max-width: ${(p) => p.theme.breakpoints.md}) {
    transform: rotate(135deg) translateY(-5px) translateX(6px) !important;
  }
`;
interface Props {
  challenge: Challenge;
  lang: string;
}

const ButtonsContainer = styled(Box)`
  > button {
    margin-bottom: 5px;
  }
`;
const ChallengeHeader: FC<Props> = ({ challenge, lang = 'en' }) => {
  const { t } = useTranslation('common');
  const user = useContext(UserContext);
  const router = useRouter();
  const { userData } = useUserData();
  const { showModal, closeModal } = useModal();
  const theme = useTheme();
  const { data: projectsData, mutate: mutateProjects } = useGet<{
    projects: Project[];
  }>(`/api/challenges/${challenge.id}/projects`);
  // const activeSpaces = challenge.spaces[0].filter((space) => space.affiliation_status !== 'pending');
  const activeSpace = challenge.spaces[0] && challenge.spaces[0][0];
  const customChalName =
    challenge.program.custom_challenge_name || (challenge.spaces[0] && challenge.spaces[0][0].custom_challenge_name);

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  const Stats = ({ value, title }) => (
    <Box justifyContent="center" alignItems="center">
      <div>{value}</div>
      <div>{title}</div>
    </Box>
  );

  const {
    has_followed,
    has_saved,
    id,
    is_member,
    is_owner,
    members_count,
    projects_count,
    needs_count,
    status,
    title,
    title_fr,
    program,
    short_description,
    short_description_fr,
  } = challenge;

  // dynamically set challengeStatus to completed if date is over and admin forgot to set it as complete, else show normal status
  // const challengeStatus = final_date && getDaysLeft(final_date) === 0 ? 'completed' : status;
  const statusStep = statusToStep(status);
  return (
    <div className="challengeHeader">
      <Box px={[3, undefined, undefined, undefined, 0]}>
        <Box alignItems="center" width={'full'} mb={[6, undefined, undefined, 8]} pt={5}>
          <Box row justifyContent="center" alignItems="center" flexWrap="wrap" textAlign="center">
            <H1 fontSize={['2.55rem', undefined, '3rem']}>
              {/* different naming for challenge if program admin has set one */}
              {customChalName ? customChalName.slice(0, -1) : t('challenge.title')}
              &nbsp;
              <span>{`{${(lang === 'fr' && title_fr) || title}}`}</span>
            </H1>
            <Box row pl={5} alignSelf="center">
              {userData && (
                <>
                  <Box>
                    <BtnSave itemType="challenges" itemId={id} saveState={has_saved} />
                  </Box>
                </>
              )}
              <ShareBtns type="challenge" />
            </Box>
          </Box>

          {/* edit button */}
          {challenge.is_admin && (
            <Box mt={4}>
              <Link href={`/challenge/${challenge.short_title}/edit`}>
                <a style={{ display: 'flex', justifyContent: 'center' }}>
                  <Edit size={23} title="Edit challenge" />
                  {t('entity.form.btnAdmin')}
                </a>
              </Link>
            </Box>
          )}
        </Box>
        <Grid display="grid" gridTemplateColumns={['100%', undefined, undefined, '22% 50% 28%']}>
          <Box
            row
            alignSelf="flex-start"
            alignItems="center"
            justifyContent="center"
            justifySelf="center"
            spaceX={4}
            flexWrap="wrap"
            order={[2, undefined, undefined, 1]}
          >
            {program.id !== -1 && ( // if program id is !== -1 (meaning challenge is not attached to a program), display program name and link
              <Box row pb={3}>
                <strong>{t('entity.info.program_title')}&nbsp;</strong>
                <Link href={`/program/${program.short_title}`}>
                  <a>{program.title}</a>
                </Link>
              </Box>
            )}
            {activeSpace && (
              <Box row pb={3}>
                <strong>{t('space.title')}:&nbsp;</strong>
                <Link href={`/space/${activeSpace.short_title}`}>
                  <a>{activeSpace.title}</a>
                </Link>
              </Box>
            )}
            {status !== 'draft' && (
              <Box textAlign={['center', undefined, 'inherit']}>
                <Grid
                  display="grid"
                  gridTemplateColumns={['27% 53%', undefined, undefined, '33% 53%']}
                  justifyContent={['center', undefined, undefined, 'flex-start']}
                  alignItems="center"
                  width="12rem"
                  margin={['auto', undefined, undefined, 'inherit']}
                  style={{ paddingBottom: '15px' }}
                >
                  <>
                    {/* where statusStep[0] = step number, and statusStep[1] = step color */}
                    <StatusCircle mr={2} step={statusStep[0]}>
                      <StatusStep>{statusStep[0]}/4</StatusStep>
                    </StatusCircle>
                    <Box color={statusStep[1]}>{t(`challenge.info.status_${status}`)}</Box>
                  </>
                </Grid>
              </Box>
            )}
            {/* <span tw="text-gray-500">{t('challenge.info.status_draft')}</span> */}
          </Box>
          <Box textAlign="center" px={4} order={[1, undefined, undefined, 2]}>
            <P fontSize="17.5px">{(lang === 'fr' && short_description_fr) || short_description}</P>
          </Box>
          <Box order={3} pt={[4, undefined, undefined, 0]}>
            <ButtonsContainer
              row
              spaceX={2}
              mb={5}
              justifyContent={['center', undefined, undefined, 'flex-end']}
              alignItems="center"
              flexWrap="wrap"
            >
              <BtnFollow followState={has_followed} itemType="challenges" itemId={challenge.id} />
              {!is_owner && ( // show join button only if we are not owner of challenge
                <BtnJoin
                  joinState={is_member}
                  itemType="challenges"
                  itemId={id}
                  textJoin={t('challenge.info.btnJoin')}
                  textUnjoin={t('challenge.info.btnUnjoin')}
                  // onJoin={() => router.push(`/challenges/${id}`)}
                />
              )}
              {/* show submit button only for connected users, and only if status of challenge is accepting projects + challenge is attached to a program */}
              {user.isConnected && status === 'accepting' && (program.id !== -1 || activeSpace) && (
                <Button
                  // eslint-disable-next-line react/jsx-no-bind
                  onClick={() => {
                    showModal({
                      children: (
                        <ProjectLinkModal
                          alreadyPresentProjects={projectsData?.projects}
                          challengeId={id}
                          programId={program.id}
                          mutateProjects={mutateProjects}
                          // eslint-disable-next-line react/jsx-no-bind
                          closeModal={closeModal}
                          isMember={is_member}
                        />
                      ),
                      title: t('attach.project.title'),
                      maxWidth: '50rem',
                    });
                  }}
                >
                  {t('attach.project.title')}
                </Button>
              )}
            </ButtonsContainer>

            <Box
              textAlign="center"
              alignItems="center"
              justifyContent="space-around"
              spaceX={[4, 6]}
              row
              pb={2}
              px={[0, undefined, 2]}
              width={['100%', '30rem', undefined, '100%']}
              alignSelf="center"
            >
              <A href={`/challenge/${router.query.short_title}?tab=projects`} shallow noStyle scroll={false}>
                <Stats value={projects_count} title={<TextWithPlural type="project" count={projects_count} />} />
              </A>
              <Box borderRight={`2px solid ${theme.colors.greys['400']}`} height={8} />
              <A href={`/challenge/${router.query.short_title}?tab=needs`} shallow noStyle scroll={false}>
                <Stats value={needs_count} title={<TextWithPlural type="need" count={needs_count} />} />
              </A>
              <Box borderRight={`2px solid ${theme.colors.greys['400']}`} height={8} />
              <A href={`/challenge/${router.query.short_title}?tab=members`} shallow noStyle scroll={false}>
                <Stats value={members_count} title={<TextWithPlural type="member" count={members_count} />} />
              </A>
            </Box>
          </Box>
        </Grid>
      </Box>
    </div>
  );
};
export default ChallengeHeader;
