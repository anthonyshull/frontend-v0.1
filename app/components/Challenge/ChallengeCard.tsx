/* eslint-disable camelcase */
import Link from 'next/link';
import React, { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import Box from '~/components/Box';
import Card from '~/components/Card';
import H2 from '~/components/primitives/H2';
import Title from '~/components/primitives/Title';
import BtnSave from '~/components/Tools/BtnSave';
import { DataSource, Program, Space } from '~/types';
import { TextWithPlural } from '~/utils/managePlurals';
import { statusToStep } from './ChallengeStatus';
import ReactTooltip from 'react-tooltip';

export type Challenge = {
  id: number;
  title: string;
  title_fr?: string;
  short_title: string;
  short_description: string;
  short_description_fr?: string;
  clapsCount: number;
  membersCount: number;
  projectsCount: number;
  program?: Pick<Program, 'id' | 'short_title' | 'title' | 'title_fr'>;
  space?: Pick<Space, 'id' | 'short_title' | 'title' | 'title_fr'>;
  needsCount: number;
  has_saved: boolean;
  banner_url: string;
  status: string;
};
interface Props extends Challenge {
  width?: string;
  source?: DataSource;
  cardFormat?: string;
}
const ChallengeCard: FC<Props> = ({
  id,
  title,
  title_fr,
  short_title,
  short_description,
  short_description_fr,
  clapsCount,
  membersCount,
  projectsCount,
  program,
  space,
  needsCount = 0,
  has_saved,
  banner_url = '/images/default/default-challenge.jpg',
  width,
  source,
  cardFormat,
  status,
}) => {
  const router = useRouter();
  const { locale } = router;
  const { t } = useTranslation('common');
  const challengeUrl = `/challenge/${short_title}`;
  const statusStep = statusToStep(status);
  const TitleFontSize = cardFormat !== 'compact' ? ['4xl', '5xl'] : '3xl';
  const affSpace = (space && space[0] && space[0][0]) || [];

  return (
    <Card imgUrl={banner_url} href={challengeUrl} width={width}>
      <Box row justifyContent="space-between" spaceX={4}>
        <Link href={challengeUrl} passHref>
          <Title tw="pr-2">
            <H2 fontSize={TitleFontSize}>{(locale === 'fr' && title_fr) || title}</H2>
          </Title>
        </Link>
        <BtnSave itemType="challenges" itemId={id} saveState={has_saved} source={source} />
      </Box>
      {/* <div style={{ color: "grey", paddingBottom: "5px" }}>
        <span> Last active today </span> <span> Prototyping </span>
      </div> */}
      {program &&
        program.id !== -1 && ( // if program id is !== -1 (meaning challenge is not attached to a program), display program name and link
          <Box row mt={0}>
            {t('entity.info.program_title')}
            &nbsp;
            <Link href={`/program/${program.short_title}`}>
              <a>{(locale === 'fr' && program.title_fr) || program.title}</a>
            </Link>
          </Box>
        )}
      {affSpace.length !== 0 && ( // if program id is !== -1 (meaning challenge is not attached to a program), display program name and link
        <Box row mt={0}>
          {t('space.title')}: &nbsp;
          <Link href={`/space/${affSpace.short_title}`}>
            <a>{(locale === 'fr' && affSpace.title_fr) || affSpace.title}</a>
          </Link>
        </Box>
      )}
      <Box
        style={{ color: status !== 'draft' ? statusStep[1] : 'grey' }}
        row
        width="fit-content"
        borderRadius={6}
        mt={2}
        alignItems="center"
        data-tip={t('entity.info.status.title')}
        data-for="challengeCard_status"
      >
        <Box>{t(`challenge.info.status_${status}`)}</Box>
      </Box>
      <ReactTooltip id="challengeCard_status" effect="solid" />
      <div tw="line-clamp-4 flex-1">{(locale === 'fr' && short_description_fr) || short_description}</div>
      <Box row alignItems="center" justifyContent="space-between" spaceX={2} flexWrap="wrap">
        <CardData value={membersCount} title={<TextWithPlural type="member" count={membersCount} />} />
        <CardData value={needsCount} title={<TextWithPlural type="need" count={needsCount} />} />
        <CardData value={projectsCount} title={<TextWithPlural type="project" count={projectsCount} />} />
      </Box>
    </Card>
  );
};

const CardData = ({ value, title }) => (
  <Box justifyContent="center" alignItems="center">
    <div>{value}</div>
    <div>{title}</div>
  </Box>
);

export default ChallengeCard;
