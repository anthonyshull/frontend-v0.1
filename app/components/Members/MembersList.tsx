import useTranslation from 'next-translate/useTranslation';
import React, { FC, useCallback, useEffect, useState } from 'react';
import Loading from '~/components/Tools/Loading';
import { useApi } from '~/contexts/apiContext';
import { useModal } from '~/contexts/modalContext';
import { ItemType } from '~/types';
import Grid from '../Grid';
import Button from '../primitives/Button';
import MemberCard from './MemberCard';
import MembersModal from './MembersModal';
import DropdownRole from '../Tools/DropdownRole';
import SpinLoader from '../Tools/SpinLoader';

// import "./MembersList.scss";

interface Props {
  itemId: number;
  itemType: ItemType;
  isOwner: boolean;
}
const MembersList: FC<Props> = ({ itemId = 0, itemType, isOwner = false }) => {
  const [listMembers, setListMembers] = useState([]);
  const [selectedMembers, setSelectedMembers] = useState([]);
  const [showChangedMessage, setShowChangedMessage] = useState({ changing: false, ids: [] });
  const [sending, setSending] = useState(false);
  const [newRole, setNewRole] = useState({});
  const [getDeleted, setGetDeleted] = useState(false);
  const api = useApi();
  const { showModal } = useModal();
  const { t } = useTranslation('common');
  const getMembers = useCallback(() => {
    if (itemId) {
      api
        .get(`/api/${itemType}/${itemId}/members`)
        .then((res) => {
          // filter members by type
          const owners = res.data.members.filter((member) => member.owner);
          const admins = res.data.members.filter((member) => member.admin && !member.owner);
          const members = res.data.members.filter((member) => !member.owner && !member.admin && member.member);
          // get potential pending members
          api
            .get(`/api/${itemType}/${itemId}/members?status=pending`)
            .then((res) => {
              const pending = res.data.members;
              // group/order all members by displaying pending first (if exist), then owners, admins and members (independent of if object has pending members)
              setListMembers([...pending, ...owners, ...admins, ...members]);
            })
            .catch((err) => console.error(`Couldn't GET pending members of ${itemType} with itemId=${itemId}`, err));
        })
        .catch((err) => console.error(`Couldn't GET members of ${itemType} with itemId=${itemId}`, err));
    }
  }, [api, itemId, itemType]);
  useEffect(() => {
    getMembers();
  }, [getMembers]);

  const handleMembers = () => {
    if (!selectedMembers.length || selectedMembers.length !== listMembers.length - 1) {
      const selectedMembersIds = selectedMembers.map((m) => m.id);
      for (let i = 0, len = listMembers.length; i < len; i++) {
        if (!selectedMembersIds.includes(listMembers[i].id)) {
          if (getRole(listMembers[i]) !== 'owner') setSelectedMembers((old) => [...old, listMembers[i]]);
        }
      }
    } else if (selectedMembers.length === listMembers.length - 1) setSelectedMembers([]);
  };

  const getRole = (member) => {
    let actualRole = 'member';
    if (!member.owner && !member.admin && !member.member) {
      actualRole = 'pending';
    }
    if (member.member) {
      actualRole = 'member';
    }
    if (member.owner) {
      actualRole = 'owner';
    } else if (member.admin) {
      actualRole = 'admin';
    }
    return actualRole;
  };

  const onRoleChanged = (member) => {
    if (Array.isArray(member)) {
      setShowChangedMessage({ changing: true, ids: member.map((m) => m.id) });
    }
    setShowChangedMessage({ changing: true, ids: [member.id] });
    setTimeout(() => {
      setShowChangedMessage({ changing: false, ids: [] });
    }, 1500);
  };

  const removeOrRejectMember = () => {
    setSending(true);
    for (let i = 0, len = selectedMembers.length; i < len; i++) {
      api
        .delete(`/api/${itemType}/${itemId}/members/${selectedMembers[i].id}`)
        .then(() => {
          setSending(false);
        })
        .catch(() => setSending(false));
      // setTimeout is used to delay the update of listMembers state after processing loop requests
      if (i === selectedMembers.length - 1) {
        setListMembers([]);
        setTimeout(() => getMembers(), 1000);
      }
    }
    setSelectedMembers([]);
    setGetDeleted(false);
  };

  const handleChange = () => {
    if (newRole.value) {
      if (itemId || itemType || selectedMembers.length) {
        for (let i = 0; i < selectedMembers.length; i++) {
          const jsonToSend = {
            user_id: selectedMembers[i].id,
            previous_role: getRole(selectedMembers[i]),
            new_role: newRole.value.toLowerCase(),
          };
          setSending(true);

          api
            .post(`/api/${itemType}/${itemId}/members`, jsonToSend)
            .then(() => setSending(false))
            .catch((err) => {
              console.error(`Couldn't POST ${itemType} with itemId=${itemId} members`, err);
              setSending(false);
            });
          // setTimeout is used to delay the update of listMembers state after processing loop requests
          if (i === selectedMembers.length - 1) {
            setListMembers([]);
            setTimeout(() => getMembers(), 1000);
          }
        }
      }
      setSelectedMembers([]);
      setNewRole({});
    }
  };

  const handleCancel = () => {
    setSelectedMembers([]);
    setNewRole({});
    setGetDeleted(false);
  };

  return (
    <div>
      <div className="justify-content-end" tw="flex flex-col">
        <div>
          <Button
            onClick={() => {
              showModal({
                children: <MembersModal itemType={itemType} itemId={itemId} callBack={getMembers} />,
                title: t('member.btnNewMembers.title'),
                allowOverflow: true,
              });
            }}
          >
            {t('member.btnNewMembers.title')}
          </Button>
        </div>
        {!!selectedMembers.length && (
          <div tw="flex flex-col justify-between mt-6 space-y-4 sm:(flex-row space-y-0)">
            <div tw="flex flex-auto justify-start space-x-4 sm:items-center">
              <div tw="w-48">
                <DropdownRole
                  onRoleChanged={() => onRoleChanged(selectedMembers)}
                  itemId={itemId}
                  itemType={itemType}
                  // show different list roles depending if user is owner (to prevent admins changing their roles to "owner")
                  listRole={['admin', 'member']}
                  member={selectedMembers}
                  isDisabled={true}
                  setNewRole={setNewRole}
                />
              </div>
              <div tw="flex flex-auto justify-start space-x-4">
                <Button
                  onClick={handleChange}
                  disabled={(newRole.value || getDeleted) && selectedMembers.length ? false : true}
                >
                  {sending && <SpinLoader />}
                  {t('entity.form.btnApply')}
                </Button>
                <Button
                  btnType="secondary"
                  onClick={handleCancel}
                  disabled={newRole.value || getDeleted ? false : true}
                >
                  {t('entity.form.btnCancel')}
                </Button>
              </div>
            </div>
            <Button onClick={removeOrRejectMember} btnType="danger">
              {t('general.remove')}
            </Button>
          </div>
        )}
        <div tw="flex mt-8 items-center">
          <input
            id="members"
            type="checkbox"
            tw="hover:cursor-pointer"
            onChange={handleMembers}
            checked={selectedMembers.length && selectedMembers.length === listMembers.length - 1}
          />
          <label htmlFor="members" tw="ml-4 mb-0">
            {t('general.selectMembers', { members: t('general.member_other') })}
          </label>
        </div>
      </div>
      {listMembers.length !== 0 ? (
        <Grid gridCols={1} display={['grid', 'inline-grid']} pt={6}>
          {listMembers.map((member, i) => (
            <MemberCard
              itemId={itemId}
              itemType={itemType}
              member={member}
              key={i}
              callBack={getMembers}
              isOwner={isOwner}
              selectedMembers={selectedMembers}
              setSelectedMembers={setSelectedMembers}
              getRole={getRole(member)}
              showChangedMessage={showChangedMessage}
              onRoleChanged={() => onRoleChanged(member)}
              isSending={sending}
            />
          ))}
        </Grid>
      ) : (
        <Loading />
      )}
    </div>
  );
};
export default MembersList;
