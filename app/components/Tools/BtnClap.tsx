// TODO This component over-fetches way to much data just to get the clap count.
// TODO The component should useGet for caching.
// ! This component isn't self explanatory,
import { SignLanguage } from '@emotion-icons/fa-solid/SignLanguage';
import Axios from 'axios';
import useTranslation from 'next-translate/useTranslation';
import React, { useEffect, useState } from 'react';
import { useApi } from '~/contexts/apiContext';
import useUser from '~/hooks/useUser';
import { ItemType } from '~/types';
import { logEventToGA } from '~/utils/analytics';
import SpinLoader from './SpinLoader';

interface Props {
  type?: 'text' | 'btn';
  source?: 'algolia' | 'api';
  clapState: boolean;
  clapCount?: number;
  itemId: number;
  itemType: ItemType;
  refresh?: () => {};
}
const BtnClap = ({
  type = 'btn',
  source = 'api',
  clapState: propsClapState = false,
  clapCount: propsClapCount = 0,
  itemId = undefined,
  itemType = undefined,
  refresh = undefined,
}: Props) => {
  const [clapState, setClapState] = useState(propsClapState);
  const [clapCount, setClapCount] = useState(propsClapCount);
  const [isSending, setIsSending] = useState(false);
  const [action, setAction] = useState<'clap' | 'unclap'>(propsClapState ? 'unclap' : 'clap');
  const api = useApi();
  const { user } = useUser();
  const { t } = useTranslation('common');

  useEffect(() => {
    // if data also comes from algolia (and not only api), get clap state from api (because it's not available in algolia)
    const axiosSource = Axios.CancelToken.source();
    if (source === 'algolia') {
      if (user) {
        const fetchClaps = async () => {
          const res = await api
            .get(`/api/${itemType}/${itemId}/clap`, { cancelToken: axiosSource.token })
            .catch((err) => {
              if (!Axios.isCancel(err)) {
                console.error("Couldn't GET claps", err);
              }
            });
          if (res?.data?.has_clapped) {
            setClapState(res.data.has_clapped);
          }
        };
        fetchClaps();
      }
      setClapState(false);
    }
    return () => {
      // This will prevent to setClapState on unmounted BtnClap
      axiosSource.cancel();
    };
  }, [api, itemId, itemType, source, user]);

  useEffect(() => {
    setAction(clapState ? 'unclap' : 'clap');
  }, [clapState]);

  const changeStateClap = (e) => {
    // toggle display of comments unless we don't want the comments to show at all (in post card)
    if (itemId && itemType && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      setIsSending(true);
      // send event to google analytics
      user &&
        logEventToGA(action, 'Button', `[${user.id},${itemId},${itemType}]`, { userId: user.id, itemId, itemType });
      const newClapCount = clapState ? clapCount - 1 : clapCount + 1;
      if (action === 'clap') {
        api
          .put(`/api/${itemType}/${itemId}/clap`)
          .then(() => {
            setClapCount(newClapCount);
            setClapState(true);
            setIsSending(false);
            refresh();
          })
          .catch(() => {
            setIsSending(false);
          });
      } else {
        api
          .delete(`/api/${itemType}/${itemId}/clap`)
          .then(() => {
            setClapCount(newClapCount);
            setClapState(false);
            setIsSending(false);
            refresh();
          })
          .catch(() => {
            setIsSending(false);
          });
      }
    }
  };
  if (type === 'text') {
    return (
      <div
        onClick={changeStateClap}
        onKeyUp={changeStateClap}
        className={clapState ? 'btn-postcard btn text-primary' : 'btn-postcard btn text-dark'}
        disabled={isSending}
        role="button"
        tabIndex={0}
      >
        {isSending ? <SpinLoader /> : <SignLanguage size={20} title="Clap" />}
        &nbsp;
        {t('post.clap')}
      </div>
    );
  }
  return (
    <>
      <button
        onClick={changeStateClap}
        onKeyUp={changeStateClap}
        className={
          clapState ? 'btn-clap btn btn-sm btn-primary text-light' : 'btn-clap btn btn-sm btn-secondary text-dark'
        }
        type="button"
        id="btnClap"
        disabled={isSending}
        // data-tip={action}
        // data-for="clapBtn"
      >
        <SignLanguage size={20} />
        <span>{clapCount}</span>
        {isSending && <SpinLoader />}
      </button>
      {/* <ReactTooltip id="clapBtn" delayHide={300} effect="solid" /> */}
    </>
  );
};

export default BtnClap;
