// TODO This component over-fetches way to much data just to get the save count.
// TODO The component should useGet for caching.
// ! This component isn't self explanatory,
import Axios from 'axios';
import React, { FC, useEffect, useState } from 'react';
import styled from '@emotion/styled';
import { Bookmark as FilledBookmark } from '@emotion-icons/fa-solid/Bookmark';
import { Bookmark as OutlinedBookmark } from '@emotion-icons/fa-regular/Bookmark';
import { EmotionIconBase } from '@emotion-icons/emotion-icon';
import { useApi } from '~/contexts/apiContext';
import { DataSource, ItemType } from '~/types';
import ReactTooltip from 'react-tooltip';
import useTranslation from 'next-translate/useTranslation';
import useUser from '~/hooks/useUser';
import SpinLoader from './SpinLoader';
import { logEventToGA } from '~/utils/analytics';

interface Props {
  source?: DataSource;
  saveState: boolean;
  itemId: number;
  itemType: ItemType;
  refresh?: () => void;
}

const BookmarkStyleWrapper = styled.div`
  ${EmotionIconBase} {
    opacity: 0.8;
    cursor: pointer;
    color: #4c4c4c;
    width: 22px;
    height: 22px;
  }
`;

const saveStateIcon = {
  unsaved: <OutlinedBookmark title="Unsave" />,
  saved: <FilledBookmark title="Save" />,
};

const BtnSave: FC<Props> = ({ source = 'api', saveState: propsSaveState = false, itemId, itemType, refresh }) => {
  const [saveState, setSaveState] = useState(propsSaveState);
  const [action, setAction] = useState<'save' | 'unsave'>(propsSaveState ? 'unsave' : 'save');
  const [icon, setIcon] = useState<'saved' | 'unsaved'>(saveState ? 'saved' : 'unsaved');
  const [sending, setSending] = useState(false);
  const api = useApi();
  const { t } = useTranslation('common');
  const { user } = useUser();
  useEffect(() => {
    // if data also comes from algolia (and not only api), get save state from api (because it's not available in algolia)
    const axiosSource = Axios.CancelToken.source();
    if (source === 'algolia') {
      if (user) {
        const fetchSaves = async () => {
          const res = await api
            .get(`/api/${itemType}/${itemId}/save`, { cancelToken: axiosSource.token })
            .catch((err) => {
              if (!Axios.isCancel(err)) {
                console.error("Couldn't GET saves", err);
              }
            });
          if (res?.data?.has_saved) {
            setSaveState(res.data.has_saved);
          }
        };
        fetchSaves();
      }
      setSaveState(false);
    }
    return () => {
      // This will prevent to setSaveState on unmounted BtnSave
      axiosSource.cancel();
    };
  }, [api, itemId, itemType, source, user]);

  useEffect(() => {
    setAction(saveState ? 'unsave' : 'save');
  }, [saveState]);

  const changeStateSave = (e) => {
    if (itemId && itemType && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      setSending(true);
      if (action === 'save') {
        api
          .put(`/api/${itemType}/${itemId}/save`)
          .then(() => {
            // send event to google analytics
            logEventToGA(action, 'Button', `[${user.id},${itemId},${itemType}]`, { userId: user.id, itemId, itemType });
            setSaveState(true);
            setSending(false);
          })
          .catch(() => setSending(false));
      } else {
        api.delete(`/api/${itemType}/${itemId}/save`).then(() => {
          setSaveState(false);
          setSending(false);
        });
      }
      refresh && refresh();
    }
  };
  return (
    <>
      {sending ? (
        <SpinLoader />
      ) : (
        <>
          <a
            tabIndex={0}
            onMouseEnter={() => setIcon(saveState ? 'unsaved' : 'saved')}
            onMouseLeave={() => setIcon(saveState ? 'saved' : 'unsaved')}
            onClick={changeStateSave}
            onKeyUp={changeStateSave}
            data-tip={t(`general.${action}`)}
            data-for="save"
            aria-describedby={`save ${itemType}`}
          >
            <BookmarkStyleWrapper>{saveStateIcon[icon]}</BookmarkStyleWrapper>
          </a>
          <ReactTooltip id="save" delayHide={300} effect="solid" role="tooltip" />
        </>
      )}
    </>
  );
};

export default BtnSave;
