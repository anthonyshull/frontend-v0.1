import React, { FC, ReactNode, useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useApi } from '~/contexts/apiContext';
import { DataSource, ItemType } from '~/types';
import Button from '../primitives/Button';
import useUser from '~/hooks/useUser';
import Axios from 'axios';
import SpinLoader from './SpinLoader';
import { logEventToGA } from '~/utils/analytics';

interface Props {
  btnType?: string;
  roundButton?: boolean;
  followState: boolean;
  itemType: ItemType;
  itemId: number;
  textFollow?: string | ReactNode;
  textUnfollow?: string | ReactNode;
  source?: DataSource;
  width?: string;
}

const BtnFollow: FC<Props> = ({
  btnType = 'secondary',
  roundButton = false,
  followState: followStateProp = false,
  itemType = undefined,
  itemId = undefined,
  textFollow = undefined,
  textUnfollow = undefined,
  source = 'api',
  width,
}) => {
  const [followState, setFollowState] = useState(followStateProp);
  const [sending, setSending] = useState(false);
  const api = useApi();
  const { t } = useTranslation('common');
  const { user } = useUser();

  textFollow = textFollow || t('general.follow');
  textUnfollow = textUnfollow || t('general.unfollow');

  useEffect(() => {
    // if data also comes from algolia (and not only api), get save state from api (because it's not available in algolia)
    const axiosSource = Axios.CancelToken.source();
    if (source === 'algolia') {
      if (user) {
        const fetchSaves = async () => {
          const res = await api
            .get(`/api/${itemType}/${itemId}/follow`, { cancelToken: axiosSource.token })
            .catch((err) => {
              if (!Axios.isCancel(err)) {
                console.error("Couldn't GET saves", err);
              }
            });
          if (res?.data?.has_followed) {
            setFollowState(res.data.has_followed);
          }
        };
        fetchSaves();
      }
      setFollowState(false);
    }
    return () => {
      // This will prevent to setFollowState on unmounted BtnSave
      axiosSource.cancel();
    };
  }, [api, itemId, itemType, source, user]);

  const changeStateFollow = (e) => {
    if (itemId && itemType && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      setSending(true);
      const action = followState ? 'unfollow' : 'follow';
      // send event to google analytics
      user &&
        logEventToGA(action, 'Button', `[${user.id},${itemId},${itemType}]`, { userId: user.id, itemId, itemType });
      if (action === 'follow') {
        // if user is following
        api
          .put(`/api/${itemType}/${itemId}/follow`)
          .then(() => {
            setFollowState((prevState) => !prevState);
            setSending(false);
          })
          .catch((err) => {
            console.error(`Couldn't PUT /api/${itemType}/${itemId}/follow`, err);
            setSending(false);
          });
      } else {
        // else unfollow
        api
          .delete(`/api/${itemType}/${itemId}/follow`)
          .then(() => {
            setFollowState((prevState) => !prevState);
            setSending(false);
          })
          .catch((err) => {
            console.error(`Couldn't DELETE /api/${itemType}/${itemId}/follow`, err);
            setSending(false);
          });
      }
    }
  };
  const text = followState ? textUnfollow : textFollow;
  return (
    <Button
      onClick={changeStateFollow}
      onKeyUp={changeStateFollow}
      btnType={btnType}
      disabled={sending}
      width={width}
      // {...(roundButton && { style: { borderRadius: '50px' } })}
    >
      {sending && <SpinLoader />}
      {text}
    </Button>
  );
};
export default BtnFollow;
