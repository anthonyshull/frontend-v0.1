import useTranslation from 'next-translate/useTranslation';
import { Twitter, Facebook, Linkedin, Whatsapp, Telegram, Reddit } from '@emotion-icons/fa-brands';
import { Envelope, Link, Share, ShareAlt } from '@emotion-icons/fa-solid';
import { useRouter } from 'next/router';
import { useModal } from '~/contexts/modalContext';
import { copyLink } from '~/utils/utils';
import useUser from '~/hooks/useUser';
import { ItemType } from '~/types';
import { FC } from 'react';
import { logEventToGA } from '~/utils/analytics';

interface Props {
  type: ItemType;
  specialObjId?: number;
}

const ShareBtns: FC<Props> = ({ type, specialObjId }) => {
  const router = useRouter();
  const { t } = useTranslation('common');
  const { user } = useUser();
  const { showModal } = useModal();

  const onShareButtonClick = (e) => {
    // set objectId depending on type
    const objectId =
      type === 'post' || type === 'need'
        ? specialObjId
        : type === 'challenge' || type === 'program' || type === 'space'
        ? router.query.short_title
        : router.query.id;
    // make objUrlPath the router asPath, except if we are sharing need or posts, in which case the url is the one from the single need or post page
    const objUrlPath = type === 'need' ? `/need/${objectId}` : type === 'post' ? `/post/${objectId}` : router.asPath;
    // check if browser/phone support the native share api (meaning we are in a mobile)
    if (navigator.share) {
      navigator
        .share({
          title: `${type}`,
          text: t('general.shareBtns.text', { type: t(`${type}.title`) }),
          url: process.env.ADDRESS_FRONT + objUrlPath,
        })
        .catch(console.error);
    } else {
      // else open modal with defined sharing methods
      if ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) {
        // if function is launched via keypress, execute only if it's the 'enter' key
        showModal({
          children: <Modal type={type} objectId={objectId} objUrlPath={objUrlPath} userId={user?.id} t={t} />,
          title: t('general.shareBtns.modalTitle', { type: t(`${type}.title`) }),
        });
      }
    }
    // send event to google analytics
    logEventToGA('Open share modal', 'Share button', `[${user?.id},${objectId},${type}]`, {
      userId: user?.id,
      itemId: objectId,
      itemType: type,
    });
  };

  return (
    <div className="shareBtns">
      <button
        className={
          type === 'post' || type === 'need'
            ? 'btn-postcard'
            : type === 'program' || type === 'challenge' || type === 'space'
            ? 'share-button-bg special'
            : 'share-button-bg'
        }
        type="button"
        title={`Share this ${type}`}
        onClick={onShareButtonClick}
        onKeyUp={onShareButtonClick}
        tabIndex={0}
      >
        {type === 'project' || type === 'community' ? (
          <ShareAlt size={18} title="Share to social media" />
        ) : (
          <Share size={18} title="Share to social media" />
        )}
        {type !== 'program' && type !== 'challenge' && type !== 'space' && (
          <span>{t('general.shareBtns.btnTitle')}</span>
        )}
      </button>
    </div>
  );
};

const Modal = ({ type, objectId, objUrlPath, userId, t }) => {
  const objUrl = process.env.ADDRESS_FRONT + encodeURIComponent(objUrlPath);
  const openWindow = (socialMedia) => {
    // send event to google analytics
    logEventToGA('Desktop share', 'Share button', `[${userId},${objectId},${type},${socialMedia}]`, {
      userId,
      itemId: objectId,
      itemType: type,
      socialMedia,
    });
    let shareUrl;
    if (socialMedia === 'facebook') shareUrl = `https://www.facebook.com/sharer/sharer.php?u=${objUrl}`;
    if (socialMedia === 'twitter')
      shareUrl = `https://twitter.com/intent/tweet/?text=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform:&hashtags=JOGL&url=${objUrl}`;
    if (socialMedia === 'linkedin')
      shareUrl = `https://www.linkedin.com/shareArticle?mini=true&url=${objUrl}&title=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform&amp;&source=${objUrl}`;
    if (socialMedia === 'reddit')
      shareUrl = `https://reddit.com/submit/?url=${objUrl}&resubmit=true&amp;title=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform:&amp;`;
    if (socialMedia === 'whatsapp')
      shareUrl = `https://api.whatsapp.com/send?text=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform: ${objUrl}&preview_url=true`;
    if (socialMedia === 'telegram')
      shareUrl = `https://telegram.me/share/url?text=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform:&amp;url=${objUrl}`;
    window.open(shareUrl, 'share-dialog', 'width=626,height=436');
  };
  return (
    <div className="share-dialog is-open">
      <div className="targets">
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('facebook')}
          aria-label="Share on Facebook"
        >
          <div className="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--large">
            <Facebook size={19} title="Facebook" /> Facebook
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('twitter')}
          aria-label="Share on Twitter"
        >
          <div className="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--large">
            <Twitter size={19} title="Twitter" /> Twitter
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('linkedin')}
          aria-label="Share on LinkedIn"
        >
          <div className="resp-sharing-button resp-sharing-button--linkedin resp-sharing-button--large">
            <Linkedin size={19} title="Linkedin" /> LinkedIn
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('reddit')}
          aria-label="Share on Reddit"
        >
          <div className="resp-sharing-button resp-sharing-button--reddit resp-sharing-button--large">
            <Reddit size={19} title="Reddit" /> Reddit
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('whatsapp')}
          aria-label="Share on WhatsApp"
        >
          <div className="resp-sharing-button resp-sharing-button--whatsapp resp-sharing-button--large">
            <Whatsapp size={19} title="Whatsapp" />
            Whatsapp
          </div>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => openWindow('telegram')}
          aria-label="Share on Telegram"
        >
          <div className="resp-sharing-button resp-sharing-button--telegram resp-sharing-button--large">
            <Telegram size={19} title="Telegram" /> Telegram
          </div>
        </button>
        <button type="button" className="resp-sharing-button__link" rel="noopener" aria-label="Share by E-Mail">
          <a
            href={`mailto:?subject=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform&body=${objUrl}`}
            target="_self"
          >
            <div className="resp-sharing-button resp-sharing-button--email resp-sharing-button--large">
              <Envelope size={19} title="Envelope" /> Email
            </div>
          </a>
        </button>
        <button
          type="button"
          className="resp-sharing-button__link"
          onClick={() => copyLink(objectId, type, undefined, t)}
          rel="noopener"
          aria-label="Copy link"
        >
          <div className="resp-sharing-button resp-sharing-button--link resp-sharing-button--large">
            <Link size={19} title="Copy link" /> {t('general.copyLink')}
          </div>
        </button>
      </div>
    </div>
  );
};

export default ShareBtns;
