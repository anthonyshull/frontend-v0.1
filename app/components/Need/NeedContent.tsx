import { Edit } from '@emotion-icons/boxicons-solid/Edit';
import { ExternalLinkAlt } from '@emotion-icons/fa-solid/ExternalLinkAlt';
import Link from 'next/link';
import React, { FC, useContext } from 'react';
import useTranslation from 'next-translate/useTranslation';
import Feed from '~/components/Feed/Feed';
import BtnFollow from '~/components/Tools/BtnFollow';
import BtnJoin from '~/components/Tools/BtnJoin';
import ShareBtns from '~/components/Tools/ShareBtns/ShareBtns';
import { UserContext } from '~/contexts/UserProvider';
import useUserData from '~/hooks/useUserData';
import { NeedDisplayMode } from '~/pages/need/[id]';
import Box from '../Box';
import BtnSave from '../Tools/BtnSave';
import InfoHtmlComponent from '../Tools/Info/InfoHtmlComponent';
import NeedDates from './NeedDates';
import NeedDelete from './NeedDelete';
import NeedDocsManagement from './NeedDocsManagement';
import NeedWorkers from './NeedWorkers';
import { Need } from '~/types';
import BasicChip from '../BasicChip/index';
import Chips from '../Chip/Chips';
import { theme } from 'twin.macro';

interface Props {
  changeMode: (newMode: NeedDisplayMode) => void;
  need: Need;
}
const NeedContent: FC<Props> = ({ changeMode = () => console.warn('Missing changeMode function'), need = {} }) => {
  const user = useContext(UserContext);
  if (need === undefined) {
    // eslint-disable-next-line @rushstack/no-null
    return null;
  }
  const { t } = useTranslation('common');
  const { userData } = useUserData();
  return (
    <div className={`needContent need${need.id}`}>
      <div className="needContent--header d-flex justify-content-between">
        <div className="d-block">
          <h4>{need.title}</h4>
        </div>
        <div className="need-manage right d-flex flex-row">
          {userData && (
            <>
              <Box pl={4} pr={2} alignItems="center">
                <BtnSave itemType="needs" itemId={need.id} saveState={need.has_saved} />
              </Box>
            </>
          )}
          {(need.is_admin || need.is_owner) && (
            <div className="btn-group dropright">
              <button
                type="button"
                className="btn btn-secondary dropdown-toggle"
                data-display="static"
                data-flip="false"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                •••
              </button>
              <div className="dropdown-menu dropdown-menu-right">
                <>
                  <Box
                    row
                    pb={1}
                    alignItems="center"
                    onClick={() => changeMode('update')}
                    onKeyUp={(e) => (e.which === 13 || e.keyCode === 13) && changeMode('update')}
                    tabIndex={0}
                  >
                    <Edit size={25} title="Edit need" />

                    {t('feed.object.update')}
                  </Box>
                  <NeedDelete need={need} />
                </>
              </div>
            </div>
          )}
        </div>
      </div>
      <Box flexWrap="wrap" spaceY={2} spaceX={3} row alignItems="center">
        <ShareBtns type="need" specialObjId={need.id} />
        {!need.is_owner && (
          <BtnJoin
            itemId={need.id}
            itemType="needs"
            joinState={need.is_member}
            textJoin={t('need.help.willHelp')}
            textUnjoin={t('need.help.stopHelp')}
          />
        )}
        <BtnFollow followState={need.has_followed} itemType="needs" itemId={need.id} />
        {need.status === 'completed' && <BasicChip ml={3}>{t('entity.info.status.completed')}</BasicChip>}
      </Box>
      <div tw="block mt-6">
        {t('needsPage.project')}
        <Link href={`/project/${need?.project.id}?t=needs`}>
          <a>
            {need?.project.title}{' '}
            <ExternalLinkAlt size={15} style={{ position: 'relative', top: '-2px' }} title="Go to project" />
          </a>
        </Link>
      </div>
      {/* show need publish AND/OR due date if one of them is set */}
      {(need.created_at || need.end_date) && (
        <NeedDates publishedDate={need.created_at} dueDate={need.end_date} status={need.status} />
      )}
      <span>
        {t('entity.card.by')}
        <Link href={`/user/${need.creator.id}`}>
          <a>{`${need.creator.first_name} ${need.creator.last_name}`}</a>
        </Link>
      </span>
      <div className="needContent--main">
        <InfoHtmlComponent content={need.content} />
        <hr />
        {need.skills.length !== 0 && (
          <Box>
            <Box>{t('need.skills.title')}</Box>
            <Chips
              data={need.skills.map((skill) => ({
                title: skill,
                href: `/search/needs/?refinementList[skills][0]=${skill}`,
              }))}
              color={theme`colors.lightBlue`}
              showCount={5}
              overflowText="seeMore"
            />
          </Box>
        )}
        {need.ressources.length !== 0 && (
          <Box mt={3}>
            <Box>{t('need.resources.title')}</Box>
            <Chips
              data={need.ressources.map((resource) => ({
                title: resource,
                href: `/search/needs/?refinementList[ressources][0]=${resource}`,
              }))}
              color="#eff7ff"
              showCount={5}
              overflowText="seeMore"
            />
          </Box>
        )}
        <NeedWorkers need={need} mode="details" />
        {need.documents.length > 0 && <NeedDocsManagement need={need} mode="details" />}
        <div className="needFeed">
          {(user.isConnected || (!user.isConnected && need.posts_count > 0)) && <h6>{t('need.feed.title')}</h6>}
          <Feed allowPosting={true} feedId={need.feed_id} isAdmin={need.is_owner} />
        </div>
      </div>
    </div>
  );
};

export default NeedContent;
