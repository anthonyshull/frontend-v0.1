import { FC } from 'react';
import useTranslation from 'next-translate/useTranslation';
import PostDisplay from '~/components/Feed/Posts/PostDisplay';
import PostCreate from './Posts/PostCreate';
import Alert from '~/components/Tools/Alert';
import useUserData from '~/hooks/useUserData';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';
import PostFakeLoader from './Posts/PostFakeLoader';
import useInfiniteLoading from '~/hooks/useInfiniteLoading';
interface Props {
  allowPosting: boolean;
  feedId: number | string;
  isAdmin: boolean;
  needToJoinMsg?: boolean;
}
const Feed: FC<Props> = ({ allowPosting = false, feedId, isAdmin = false, needToJoinMsg = 'undefined' }) => {
  // const [posts, setPosts] = useState([]);
  const { userData } = useUserData();
  const { t } = useTranslation('common');
  const itemsPerQuery = 5; // number of users per query calls
  const { data: dataPosts, response, error, mutate, size, setSize, isValidating } = useInfiniteLoading(
    (index) => `/api/feeds/${feedId}?items=${itemsPerQuery}&page=${index + 1}&order=desc`
  );
  const posts = dataPosts ? [].concat(...dataPosts) : [];
  const totalNumber = parseInt(response?.[0].headers['total-count']);
  const isLoadingInitialData = !dataPosts && !error;
  const isLoadingMore = isLoadingInitialData || (size > 0 && dataPosts && typeof dataPosts[size - 1] === 'undefined');
  const isEmpty = dataPosts?.[0]?.length === 0;
  const isReachingEnd = isEmpty || posts?.length === totalNumber;

  const joinMessage = needToJoinMsg ? 'general.needToJoinMsg_private' : 'general.needToJoinMsg';
  const theFeedId = feedId === 'all' ? userData?.feed_id : feedId;

  return (
    <div className="feed">
      {needToJoinMsg !== 'undefined' && !allowPosting && <Alert type="info" message={t(joinMessage)} />}
      {allowPosting && userData && (
        <PostCreate feedId={theFeedId} refresh={mutate} userImg={userData.logo_url_sm} userId={userData.id} />
      )}
      {!isLoadingInitialData ? (
        posts.length !== 0 && // show posts if there are
        posts.map((post, index) => (
          <PostDisplay post={post} key={index} feedId={theFeedId} user={userData} isAdmin={isAdmin} />
        ))
      ) : (
        // while posts are loading, show fake loader post card
        <PostFakeLoader />
      )}
      {posts.length !== 0 && !isReachingEnd && (
        <Button
          btnType="button"
          style={{ display: 'flex', margin: 'auto', justifyContent: 'center', alignItems: 'center' }}
          onClick={() => setSize(size + 1)}
          disabled={isLoadingMore || isReachingEnd}
        >
          {isLoadingMore && <SpinLoader />}
          {isLoadingMore ? t('general.loading') : !isReachingEnd ? t('general.load') : t('general.noMoreResults')}
        </Button>
      )}
    </div>
  );
};
export default Feed;
