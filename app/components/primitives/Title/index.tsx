import tw from 'twin.macro';

const Title = tw.a`cursor-pointer line-clamp-2 text-gray-700 hover:text-gray-700`;
export default Title;
