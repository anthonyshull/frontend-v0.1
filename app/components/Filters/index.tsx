import useTranslation from 'next-translate/useTranslation';
import React from 'react';
import Box from '../Box';
import FilterCheckbox from '../FilterCheckbox';
import Loading from '../Tools/Loading';

const Filters = ({
  resetButtonLabel,
  content,
  onChange,
  isError = false,
  errorMessage,
  selectedId,
  customWording = '',
}) => {
  const { t } = useTranslation('common');
  if (isError) {
    return <div>{errorMessage}</div>;
  }
  if (!content) {
    return <Loading />;
  }
  return (
    <Box row spaceX={4} width="100%" overflowX="scroll">
      <FilterCheckbox
        label={customWording ? `All ${customWording}` : t(resetButtonLabel)}
        onChange={onChange}
        checked={selectedId === undefined}
        value={undefined}
      />
      {content.map((item, index) => (
        <FilterCheckbox
          iconSrc={item.img}
          key={index}
          onChange={onChange}
          checked={item.id === selectedId}
          value={item.id}
          label={item.title}
        />
      ))}
    </Box>
  );
};

export default Filters;
