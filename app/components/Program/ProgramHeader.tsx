/* eslint-disable camelcase */
import { Edit } from '@emotion-icons/fa-solid/Edit';
import useTranslation from 'next-translate/useTranslation';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { FC, useState } from 'react';
import { display, flexbox, layout, space } from 'styled-system';
import Box from '~/components/Box';
import H1 from '~/components/primitives/H1';
import useUserData from '~/hooks/useUserData';
import { Program } from '~/types';
import { TextWithPlural } from '~/utils/managePlurals';
import styled from '~/utils/styled';
import { useTheme } from '~/utils/theme';
import A from '../primitives/A';
import BtnSave from '../Tools/BtnSave';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
// import "./ProgramHeader.scss";

const Img = styled.img`
  ${[flexbox, space, display, layout]};
  object-fit: contain;
  border-radius: 50%;
  border: 3px solid white;
  box-shadow: 0 0px 7px black;
  z-index: 1;
}
`;
interface Props {
  program: Program;
  lang: string;
}
const ProgramHeader: FC<Props> = ({ program, lang = 'en' }) => {
  const router = useRouter();
  const { userData } = useUserData();
  const [has_saved, set_has_saved] = useState(program?.has_saved);
  const theme = useTheme();
  const { t } = useTranslation('common');

  const Stats = ({ value, title }) => (
    <Box justifyContent="center" alignItems="center">
      <div>{value}</div>
      <div>{title}</div>
    </Box>
  );

  const {
    id,
    needs_count,
    // status,
    members_count,
    logo_url,
    title,
    title_fr,
    projects_count,
  } = program;

  return (
    // <Box p={[4, undefined, 2]} width="100%">
    <Box width="100%" pt={4}>
      <Box flexDirection={['row', undefined, 'column']} justifyContent="center">
        <Img
          src={logo_url || 'images/jogl-logo.png'}
          style={{ objectFit: 'contain', backgroundColor: 'white' }}
          mr={4}
          mb={[undefined, undefined, 2]}
          width={['5rem', '7rem', '9rem', '10rem']}
          height={['5rem', '7rem', '9rem', '10rem']}
          alignSelf="center"
          alt="program logo"
        />
        <Box alignItems="center">
          <Box alignItems="center" alignSelf="center">
            <H1 fontSize={['2.18rem', '2.3rem', '2.5rem', '2.78rem']} textAlign={['left', 'center']}>
              {/* display different field depending on language */}
              {(lang === 'fr' && title_fr) || title}
            </H1>
            <Box row pt={3} alignSelf={['flex-start', 'center']}>
              {userData && <BtnSave itemType="programs" itemId={id} saveState={has_saved} />}
              <ShareBtns type="program" />
            </Box>
          </Box>
          {program.is_admin && (
            <Box mt={5}>
              <Link href={`/program/${program.short_title}/edit`}>
                <a style={{ display: 'flex', justifyContent: 'center' }}>
                  <Edit size={18} title="Edit program" />
                  {t('entity.form.btnAdmin')}
                </a>
              </Link>
            </Box>
          )}
        </Box>
      </Box>
      <Box
        textAlign="center"
        alignItems="center"
        justifyContent="space-around"
        spaceX="4"
        row
        pt={[6, undefined, 8]}
        pb={2}
        px={[0, undefined, 4]}
      >
        <A href={`/program/${router.query.short_title}?tab=projects`} shallow noStyle scroll={false}>
          <Stats value={projects_count} title={<TextWithPlural type="project" count={projects_count} />} />
        </A>
        <Box borderRight={`2px solid ${theme.colors.greys['400']}`} height={8}></Box>
        <A href={`/program/${router.query.short_title}?tab=needs`} shallow noStyle scroll={false}>
          <Stats value={needs_count} title={<TextWithPlural type="need" count={needs_count} />} />
        </A>
        <Box borderRight={`2px solid ${theme.colors.greys['400']}`} height={8}></Box>
        <A href={`/program/${router.query.short_title}?tab=members`} shallow noStyle scroll={false}>
          <Stats value={members_count} title={<TextWithPlural type="member" count={members_count} />} />
        </A>
      </Box>
    </Box>
  );
};

export default ProgramHeader;
