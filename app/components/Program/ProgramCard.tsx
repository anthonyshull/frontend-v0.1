/* eslint-disable camelcase */
import Link from 'next/link';
import React from 'react';
import Box from '~/components/Box';
import Card from '~/components/Card';
import H2 from '~/components/primitives/H2';
import Title from '~/components/primitives/Title';
import BtnSave from '~/components/Tools/BtnSave';
import { TextWithPlural } from '~/utils/managePlurals';
import { useRouter } from 'next/router';
import tw, { styled } from 'twin.macro';

interface Props {
  id: number;
  title: string;
  title_fr?: string;
  short_title: string;
  short_description: string;
  short_description_fr?: string;
  membersCount?: number;
  projectsCount?: number;
  needsCount?: number;
  has_saved?: boolean;
  banner_url: string;
  source?: 'algolia' | 'api';
  cardFormat?: string;
}
const ProgramCard = ({
  id,
  title,
  title_fr,
  short_title,
  short_description,
  short_description_fr,
  membersCount,
  projectsCount,
  needsCount,
  has_saved,
  banner_url = '/images/default/default-program.jpg',
  source,
  cardFormat,
}: Props) => {
  const router = useRouter();
  const { locale } = router;
  const programUrl = `/program/${short_title}`;
  const TitleFontSize = cardFormat !== 'compact' ? ['4xl', '5xl'] : '4xl';
  return (
    <Card imgUrl={banner_url} href={programUrl}>
      <Box row justifyContent="space-between" spaceX={4}>
        <Link href={programUrl} passHref>
          <Title tw="pr-2">
            <H2 fontSize={TitleFontSize}>{(locale === 'fr' && title_fr) || title}</H2>
          </Title>
        </Link>
        {has_saved !== undefined && <BtnSave itemType="programs" itemId={id} saveState={has_saved} source={source} />}
      </Box>
      <ShortDescription cardFormat={cardFormat}>
        {(locale === 'fr' && short_description_fr) || short_description}
      </ShortDescription>
      {cardFormat !== 'compact' && (
        <Box row alignItems="center" justifyContent="space-between" spaceX={2}>
          <CardData value={membersCount} title={<TextWithPlural type="member" count={membersCount} />} />
          <CardData value={needsCount} title={<TextWithPlural type="need" count={needsCount} />} />
          <CardData value={projectsCount} title={<TextWithPlural type="project" count={projectsCount} />} />
        </Box>
      )}
    </Card>
  );
};

const ShortDescription = styled.div`
  ${tw`flex-1 color[#343a40]`}
  ${({ cardFormat }) => (cardFormat === 'compact' ? tw`line-clamp-3` : tw`line-clamp-6`)}
`;

const CardData = ({ value, title }) => (
  <Box justifyContent="center" alignItems="center">
    <div>{value}</div>
    <div>{title}</div>
  </Box>
);

export default ProgramCard;
