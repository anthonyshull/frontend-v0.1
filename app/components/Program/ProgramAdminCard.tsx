import useTranslation from 'next-translate/useTranslation';
import React, { FC, ReactNode, useEffect, useState } from 'react';
import Alert from '~/components/Tools/Alert';
import { useApi } from '~/contexts/apiContext';
import { Program } from '~/types';
import Box from '../Box';
import A from '../primitives/A';
import Button from '../primitives/Button';
import SpinLoader from '../Tools/SpinLoader';

interface Props {
  program: Program;
  spaceId: number;
  callBack: () => void;
}
const ProgramAdminCard: FC<Props> = ({ program, spaceId, callBack }) => {
  const [sending, setSending] = useState<'remove' | undefined>(undefined);
  const [error, setError] = useState<string | ReactNode>(undefined);
  const [imgToDisplay, setImgToDisplay] = useState('/images/default/default-program.jpg');
  const api = useApi();
  const { t } = useTranslation('common');

  const removeProgram = (): void => {
    setSending('remove');
    api
      .delete(`/api/spaces/${spaceId}/programs/${program.id}`)
      .then(() => {
        setSending(undefined);
        callBack();
      })
      .catch((err) => {
        console.error(err);
        setSending(undefined);
        setError(t('err-'));
      });
  };
  useEffect(() => {
    program.logo_url && setImgToDisplay(program.logo_url);
  }, [program.logo_url]);

  const bgLogo = {
    backgroundImage: `url(${imgToDisplay})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    border: '1px solid #ced4da',
    borderRadius: '50%',
    height: '50px',
    width: '50px',
  };

  if (program) {
    return (
      <div>
        <Box flexDirection={['column', 'row']} justifyContent="space-between" key={program.id}>
          <Box row alignItems="center" pb={['3', '0']} spaceX={3}>
            <div style={{ width: '50px' }}>
              <div style={bgLogo} />
            </div>
            <Box>
              <A href={`/program/${program.short_title}`}>{program.title}</A>
              <Box fontSize="85%" pt={1}>
                {t('attach.status')}
                {t(`program.info.status.${program.status}`)}
              </Box>
            </Box>
          </Box>
          <Box row alignItems="center" justifyContent={['space-between', 'flex-end']}>
            <Box pr={8}>
              {t('attach.members')}
              {program.members_count}
            </Box>
            <Button btnType="danger" disabled={sending === 'remove'} onClick={removeProgram} tw="ml-3">
              {sending === 'remove' && <SpinLoader />}
              {t('general.remove')}
            </Button>
            {error && <Alert type="danger" message={error} />}
          </Box>
        </Box>
        <hr />
      </div>
    );
  }
  // eslint-disable-next-line @rushstack/no-null
  return null;
};
export default ProgramAdminCard;
