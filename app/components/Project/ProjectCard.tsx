import Link from 'next/link';
import React, { FC } from 'react';
import { WidthProps } from 'styled-system';
import Box from '~/components/Box';
import Card from '~/components/Card';
import H2 from '~/components/primitives/H2';
import Title from '~/components/primitives/Title';
import BtnSave from '~/components/Tools/BtnSave';
import { DataSource } from '~/types';
import { TextWithPlural } from '~/utils/managePlurals';
import Chips from '../Chip/Chips';
import { theme } from 'twin.macro';
import { PatchCheckFill } from '@emotion-icons/bootstrap';
import useTranslation from 'next-translate/useTranslation';
import ReactTooltip from 'react-tooltip';

interface Props {
  id: number;
  title: string;
  shortTitle: string;
  short_description: string;
  postsCount?: number;
  followersCount?: number;
  members_count: number;
  needs_count: number;
  has_saved: boolean;
  banner_url: string;
  width?: WidthProps['width'];
  cardFormat?: string;
  source?: DataSource;
  skills?: string[];
  reviewsCount?: number;
}
const ProjectCard: FC<Props> = ({
  id,
  title,
  shortTitle = undefined,
  short_description,
  postsCount,
  followersCount,
  members_count,
  needs_count,
  has_saved,
  banner_url = '/images/default/default-project.jpg',
  width,
  cardFormat,
  source,
  skills,
  reviewsCount = 0,
}) => {
  const TitleFontSize = cardFormat !== 'compact' ? ['4xl', '5xl'] : '3xl';
  const projUrl = `/project/${id}/${shortTitle}`;
  const { t } = useTranslation('common');
  return (
    <Card imgUrl={banner_url} isImgSmall={cardFormat === 'compact'} href={projUrl} width={width}>
      <div tw="inline-flex items-center justify-between space-x-4">
        <div tw="inline-flex items-center">
          <Link href={projUrl} passHref>
            <Title tw="pr-2">
              <H2 fontSize={TitleFontSize}>{title}</H2>
            </Title>
          </Link>
          {reviewsCount === 1 && (
            <div tw="max-width[20px]">
              <PatchCheckFill
                size="15"
                title="About reviewed project"
                data-tip={t('project.info.reviewedTooltip')}
                data-for="reviewed_project"
              />
              <ReactTooltip id="reviewed_project" effect="solid" type="dark" className="forceTooltipBg" />
            </div>
          )}
        </div>
        <BtnSave itemType="projects" itemId={id} saveState={has_saved} source={source} />
      </div>
      {/* <div style={{ color: "grey", paddingBottom: "5px" }}>
            <span> Last active today </span> <span> Prototyping </span>
          </div> */}
      <div tw="line-clamp-4 flex-1">{short_description}</div>
      {skills && (
        <Chips
          data={skills.map((skill) => ({
            title: skill,
            href: `/search/projects/?refinementList[skills][0]=${skill}`,
          }))}
          overflowLink={`/project/${id}/${shortTitle}`}
          color={theme`colors.lightBlue`}
          showCount={3}
          smallChips
        />
      )}
      {cardFormat !== 'compact' && (
        <Box row alignItems="center" justifyContent="space-between" spaceX={2} flexWrap="wrap">
          <CardData value={members_count} title={<TextWithPlural type="member" count={members_count} />} />
          <CardData value={followersCount} title={<TextWithPlural type="follower" count={followersCount} />} />
          <CardData value={needs_count} title={<TextWithPlural type="need" count={needs_count} />} />
          {postsCount > 0 && <CardData value={postsCount} title={<TextWithPlural type="post" count={postsCount} />} />}
        </Box>
      )}
    </Card>
  );
};

const CardData = ({ value, title }) => (
  <Box justifyContent="center" alignItems="center">
    <div>{value}</div>
    <div>{title}</div>
  </Box>
);

export default ProjectCard;
