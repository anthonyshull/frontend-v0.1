import { Edit } from '@emotion-icons/fa-solid/Edit';
import Link from 'next/link';
import React, { FC, Fragment, useMemo } from 'react';
import useTranslation from 'next-translate/useTranslation';
import H1 from '~/components/primitives/H1';
// import BtnClap from '~/components/Tools/BtnClap';
import InfoInterestsComponent from '~/components/Tools/Info/InfoInterestsComponent';
import { useModal } from '~/contexts/modalContext';
import useMembers from '~/hooks/useMembers';
import useUserData from '~/hooks/useUserData';
import { Project } from '~/types';
import { TextWithPlural } from '~/utils/managePlurals';
import { displayObjectDate, linkify, renderOwnerNames } from '~/utils/utils';
import Box from '../Box';
import Grid from '../Grid';
import UserCard from '../User/UserCard';
import BtnFollow from '../Tools/BtnFollow';
import BtnJoin from '../Tools/BtnJoin';
import BtnSave from '../Tools/BtnSave';
import ListFollowers from '../Tools/ListFollowers';
import ShareBtns from '../Tools/ShareBtns/ShareBtns';
import QuickSearchBar from '../Tools/QuickSearchBar';
import ReactGA from 'react-ga';
import Chips from '../Chip/Chips';
// import Image from 'next/image';
import Image2 from '../Image2';
import { theme } from 'twin.macro';
import { useRouter } from 'next/router';
import useGet from '~/hooks/useGet';
import InfoDefaultComponent from '../Tools/Info/InfoDefaultComponent';
import BtnReview from '../Tools/BtnReview';
import ReactTooltip from 'react-tooltip';
import { QuestionCircle } from '@emotion-icons/fa-solid/QuestionCircle';

interface Props {
  project: Project;
}
const ProjectHeader: FC<Props> = ({ project }) => {
  const { members, membersError } = useMembers('projects', project.id, undefined);
  const { data: dataExternalLink } = useGet<{ url: string; icon_url: string }[]>(`/api/projects/${project.id}/links`);
  const { showModal } = useModal();
  const { t } = useTranslation('common');
  const { userData } = useUserData();
  const router = useRouter();
  const { locale } = router;

  const participatingToChallenges = (challenges) => {
    return (
      <p className="card-by">
        {t('project.info.participatingToChallenges')}
        {challenges.map((challenge, index) => {
          const rowLen = challenges.length;
          return (
            <Fragment key={index}>
              <Link href={`/challenge/${challenge.short_title}`}>
                <a>{(locale === 'fr' && challenge.title_fr) || challenge.title}</a>
              </Link>
              {/* add comma, except for last item */}
              {rowLen !== index + 1 && <span>, </span>}
            </Fragment>
          );
        })}
      </p>
    );
  };

  const participatingToSpaces = (spaces) => {
    return (
      <p className="card-by">
        {t('project.info.participatingToSpaces')}
        {spaces.map((space, index) => {
          return (
            <Fragment key={index}>
              <Link href={`/space/${space.short_title}`}>
                <a>{space.title}</a>
              </Link>
              {/* add comma, except for last item */}
              {spaces.length !== index + 1 && <span>, </span>}
            </Fragment>
          );
        })}
      </p>
    );
  };

  const showMembersModal = (e) => {
    if (members && ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which)) {
      // if function is launched via keypress, execute only if it's the 'enter' key
      // capture the opening of the modal as a special modal page view to google analytics
      ReactGA.modalview(`/viewMembers/project/${project.id}`);
      showModal({
        children: (
          <>
            {/* Search bar to quickly find members (show if more than 30 members) */}
            {members.length > 30 && <QuickSearchBar members={members} />}
            {/* list */}
            <Grid gridGap={[2, 4]} gridCols={[1, 2]} display={['grid', 'inline-grid']} py={[0, 2, 4]}>
              {members.map((member, i) => (
                <UserCard
                  key={i}
                  id={member.id}
                  firstName={member.first_name}
                  lastName={member.last_name}
                  nickName={member.nickname}
                  shortBio={member.short_bio}
                  logoUrl={member.logo_url}
                  skills={member.skills}
                  canContact={member.can_contact}
                  hasFollowed={member.has_followed}
                  projectsCount={member.stats.projects_count}
                  mutualCount={member.stats.mutual_count}
                  role={member.owner ? 'leader' : !member.owner && member.admin && 'admin'}
                />
              ))}
            </Grid>
          </>
        ),
        title: t('entity.tab.members'),
        maxWidth: '60rem',
      });
    }
  };
  const showFollowersModal = (e) => {
    follower_count && // open modal only if project has followers
      ((e.which && (e.which === 13 || e.keyCode === 13)) || !e.which) && // if function is launched via keypress, execute only if it's the 'enter' key
      showModal({
        children: <ListFollowers itemId={id} itemType="projects" />,
        title: t('entity.tab.followers'),
        maxWidth: '60rem',
      });
  };
  let { members_count, follower_count, banner_url } = project;
  const {
    has_followed,
    id,
    is_member,
    is_owner,
    short_description,
    title,
    interests,
    skills,
    has_clapped,
    has_saved,
    claps_count,
    users_sm,
    challenges,
    creator,
    created_at,
    updated_at,
    is_pending,
    is_private,
    reviews_count,
    affiliated_spaces,
  } = project;
  if (follower_count === undefined) follower_count = 0;
  if (members_count === undefined) members_count = 0;
  if (!banner_url) {
    banner_url = '/images/default/default-project.jpg';
  }
  const activeChallenges = challenges.filter((challenge) => challenge.project_status !== 'pending');
  // const activeSpaces = spaces[0].filter((space) => space.affiliation_status !== 'pending');
  const activeSpaces = affiliated_spaces[0] || [];
  return (
    <div className="row projectHeader">
      <Box alignItems="center" width={'full'} mb={5} px={4}>
        <Box row justifyContent="center" alignItems="center" flexWrap="wrap" textAlign="center">
          <H1 fontSize={['2.55rem', undefined, '3rem']}>{title}</H1>
          <Box row>
            {userData && (
              <>
                <div tw="pl-4">
                  <BtnSave itemType="projects" itemId={id} saveState={has_saved} />
                </div>
                {project.is_reviewer && ( // show review button only to JOGL global reviewer
                  <div tw="pl-2">
                    <BtnReview itemType="projects" itemId={id} reviewState={reviews_count > 0} />
                  </div>
                )}
              </>
            )}
            {project.reviews_count > 0 && ( // show reviewed badge if project has been reviewed
              <div tw="inline-flex items-center space-x-1 pl-5">
                <img tw="w-12" src={`/images/reviewed.png`} />
                <QuestionCircle
                  size={16}
                  color={theme`colors.secondary`}
                  title="About reviewed project"
                  data-tip={t('project.info.reviewedTooltip')}
                  data-for="reviewed_proj"
                  tabIndex={0}
                  onFocus={(e) => ReactTooltip.show(e.target)}
                  onBlur={(e) => ReactTooltip.hide(e.target)}
                />
                <ReactTooltip id="reviewed_proj" effect="solid" type="dark" className="forceTooltipBg" />
              </div>
            )}
          </Box>
        </Box>
        {/* edit button */}
        {project.is_admin && (
          <Box mt={5}>
            <Link href={`/project/${project.id}/edit`}>
              <a style={{ display: 'flex', justifyContent: 'center' }}>
                <Edit size={18} />
                {t('entity.form.btnAdmin')}
              </a>
            </Link>
          </Box>
        )}
      </Box>
      <div className="col-lg-7 col-md-12 projectHeader--banner">
        <Image2 src={banner_url} alt={title} priority />
      </div>
      <div className="col-lg-5 col-md-12 projectHeader--info">
        <div tw="font-medium">
          <InfoDefaultComponent content={linkify(short_description)} containsHtml />
        </div>
        {users_sm !== undefined && users_sm.length > 0 && renderOwnerNames(users_sm, creator, t)}
        {activeChallenges?.length !== 0 && participatingToChallenges(activeChallenges)}
        {activeSpaces?.length !== 0 && participatingToSpaces(activeSpaces)}
        <div tw="text-gray-500 inline-flex">
          {created_at && `${t('general.created_at')} ${displayObjectDate(created_at, 'LL', true)}`}
          {/* show updated date only if it exists and if it's not the same day as created date (OR if only updated_at exists and not created_at) */}
          {((updated_at && created_at && created_at.substr(0, 10) !== updated_at.substr(0, 10)) ||
            (updated_at && !created_at)) &&
            `${created_at ? ' / ' : ''} ${t('general.updated_at')} ${displayObjectDate(updated_at, 'LL', true)}`}
        </div>
        <Box pt={4} /> {/* empty div with paddingTop */}
        <InfoInterestsComponent content={interests} title={t('general.sdgs')} />
        <Box mb={3}>
          <Box color={theme`colors.secondary`}>{t('user.profile.skills')}</Box>
          <Chips
            data={skills.map((skill) => ({
              title: skill,
              href: `/search/projects/?refinementList[skills][0]=${skill}`,
            }))}
            overflowText="seeMore"
            color={theme`colors.lightBlue`}
            showCount={6}
          />
        </Box>
        <div className="projectStats">
          <span className="text" onClick={showFollowersModal} onKeyUp={showFollowersModal} tabIndex={0}>
            <strong>{follower_count}</strong>&nbsp;
            <TextWithPlural type="follower" count={follower_count} />
          </span>
          <span className="text" onClick={showMembersModal} onKeyUp={showMembersModal} tabIndex={0}>
            <strong>{members_count || 0}</strong>&nbsp;
            <TextWithPlural type="member" count={members_count} />
          </span>
        </div>
        <div tw="flex space-x-4 py-7 items-center sm:py-5">
          <BtnFollow followState={has_followed} itemType="projects" itemId={id} />
          {!is_owner && (
            <BtnJoin
              joinState={is_pending ? 'pending' : is_member}
              isPrivate={is_private}
              itemType="projects"
              itemId={id}
              textJoin={t('project.info.btnJoin')}
              textUnjoin={t('project.info.btnUnjoin')}
            />
          )}
          {/* <BtnClap itemType="projects" itemId={id} clapState={has_clapped} clapCount={claps_count} /> */}
          <ShareBtns type="project" />
        </div>
        {/* Project external links */}
        {dataExternalLink && dataExternalLink?.length !== 0 && (
          <div tw="flex flex-wrap gap-x-4 mb-5 sm:gap-x-3">
            {[...dataExternalLink].map((link, i) => (
              <a tw="items-center self-center" key={i} href={link.url} target="_blank">
                <img tw="w-10 hover:opacity-80" src={link.icon_url} />
              </a>
            ))}
          </div>
        )}
      </div>
    </div>
  );
};
export default React.memo(ProjectHeader);
