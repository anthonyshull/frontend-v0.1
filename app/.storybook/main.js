const path = require('path');

module.exports = {
  stories: ['../**/*.stories.tsx'],
  addons: [
    '@storybook/addon-knobs',
    '@storybook/addon-actions',
    '@storybook/addon-links',
    '@storybook/addon-docs',
    'storybook-addon-jsx',
  ],

  webpackFinal: async (baseConfig) => {
    const nextConfig = require('../next.config.js');
    baseConfig.resolve.alias['~/utils'] = path.resolve(__dirname, '../utils/');
    // merge whatever from nextConfig into the webpack config storybook will use
    return { ...baseConfig };
  },
};
