import { ArrowLeft } from '@emotion-icons/fa-solid/ArrowLeft';
import { TabPanel, TabPanels, Tabs } from '@reach/tabs';
import { NextPage } from 'next';
import ChallengeCreate from '~/components/Challenge/ChallengeCreate';
import { useModal } from '~/contexts/modalContext';
import ManageFaq from '~/components/Tools/ManageFaq';
import ManageResources from '~/components/Tools/ManageResources';
import ManageExternalLink from '~/components/Tools/ManageExternalLink';
import ManageBoards from '~/components/Tools/ManageBoards';
import Box from '~/components/Box';
import ChallengeAdminCard from '~/components/Challenge/ChallengeAdminCard';
import { ChallengeLinkToSpace } from '~/components/Challenge/ChallengeLinkToSpace';
import Grid from '~/components/Grid';
import Layout from '~/components/Layout';
import MembersList from '~/components/Members/MembersList';
import Button from '~/components/primitives/Button';
import H2 from '~/components/primitives/H2';
import ProjectAdminCard from '~/components/Project/ProjectAdminCard';
import { ProjectLinkToSpace } from '~/components/Project/ProjectLinkToSpace';
import SpaceForm from '~/components/Space/SpaceForm';
import DocumentsManager from '~/components/Tools/Documents/DocumentsManager';
import Loading from '~/components/Tools/Loading';
import { useApi } from '~/contexts/apiContext';
import useGet from '~/hooks/useGet';
import { Space, Project, Challenge } from '~/types';
import { NavTab, TabListStyle } from '~/components/Tabs/TabsStyles';
import useTranslation from 'next-translate/useTranslation';
import { getApiFromCtx } from '~/utils/getApi';
import { useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';

interface Props {
  space: Space;
}
const SpaceEdit: NextPage<Props> = ({ space: spaceProp }) => {
  const [space, setSpace] = useState(spaceProp);
  const [updatedSpace, setUpdatedSpace] = useState(undefined);
  const [sending, setSending] = useState(false);
  const [hasUpdated, setHasUpdated] = useState(false);
  const api = useApi();
  const { t } = useTranslation('common');
  const { showModal, closeModal } = useModal();
  const router = useRouter();
  const { data: projectsData, revalidate: projectsRevalidate, mutate: mutateProjects } = useGet<{
    projects: Project[];
  }>(`/api/spaces/${space.id}/affiliates?affiliate_type=Project`);
  const { data: challengesData, revalidate: challengesRevalidate, mutate: mutateChallenges } = useGet<{
    challenges: Challenge[];
  }>(`/api/spaces/${space.id}/affiliates?affiliate_type=Challenge`);
  const customChalName = space.custom_challenge_name;
  const customChalNameSing = customChalName ? customChalName.slice(0, -1) : t('challenge.lowerCase');

  const handleChange: (key: any, content: any) => void = (key, content) => {
    setSpace((prevSpace) => ({ ...prevSpace, [key]: content })); // update fields as user changes them
    // TODO: have only one place where we manage space content
    setUpdatedSpace((prevUpdatedSpace) => ({ ...prevUpdatedSpace, [key]: content })); // set an object containing only the fields/inputs that are updated by user
  };

  const handleChangeFeatured: (key: any, content: any) => void = (key, content) => {
    const featured_obj = key.split('.')[1];
    setSpace((prevSpace) => ({
      ...prevSpace,
      show_featured: { ...prevSpace.show_featured, [featured_obj]: content },
    })); // update fields as user changes them
    setUpdatedSpace((prevUpdatedSpace) => ({
      ...prevUpdatedSpace,
      show_featured: { ...space.show_featured, [featured_obj]: content },
    })); // set an object containing only the fields/inputs that are updated by user
  };

  const handleChangeShowTabs: (key: any, content: any) => void = (key, content) => {
    const selected_tabs = key.split('.')[1];
    setSpace((prevSpace) => ({
      ...prevSpace,
      selected_tabs: { ...prevSpace.selected_tabs, [selected_tabs]: content },
    })); // update fields as user changes them
    // TODO: have only one place where we manage space content
    setUpdatedSpace((prevUpdatedSpace) => ({
      ...prevUpdatedSpace,
      selected_tabs: { ...space.selected_tabs, [selected_tabs]: content },
    })); // set an object containing only the fields/inputs that are updated by user
  };

  const handleSubmit = async () => {
    setSending(true);
    const res = await api.patch(`/api/spaces/${space.id}`, { space: updatedSpace }).catch((err) => {
      console.error(`Couldn't patch space with id=${space.id}`, err);
      setSending(false);
    });
    if (res) {
      // on success
      setSending(false);
      setUpdatedSpace(undefined); // reset updated space component
      setHasUpdated(true); // show update confirmation message
      setTimeout(() => {
        setHasUpdated(false);
      }, 3000); // hide confirmation message after 3 seconds
    }
  };

  // Tabs elements: tabs list & handleTabsChange
  // All explained in pages-project-index file
  const tabs = [
    { value: 'general', transaltionId: 'footer.general' },
    // { value: 'home', transaltionId: 'program.home.title' },
    { value: 'about', transaltionId: 'general.tab.about' },
    { value: 'members', transaltionId: 'entity.tab.members' },
    { value: 'affiliations', transaltionId: 'general.affiliations' },
    { value: 'faqs', transaltionId: 'faq.title' },
    { value: 'partners', transaltionId: 'entity.info.enablers' },
    { value: 'resources', transaltionId: 'program.tab.resources' },
    { value: 'advanced', transaltionId: 'entity.tab.advanced' },
  ];

  const handleTabsChange = (index) => {
    const element = document.querySelector('[data-reach-tabs]');
    const headerHeight = 80;
    const y = element.getBoundingClientRect().top + window.pageYOffset - headerHeight;
    window.scrollTo({ top: y, behavior: 'smooth' });
    router.push(`/space/${space.short_title}/edit?t=${tabs[index].value}`, undefined, { shallow: true });
  };

  return (
    <Layout title={`${space.title} | JOGL`}>
      <div className="programEdit" tw="container mx-auto px-4">
        <h1>{t('space.edit.title')}</h1>
        <Link href={`/space/${space.short_title}`}>
          <a>
            <ArrowLeft size={15} title="Go back" />
            {t('space.edit.back')}
          </a>
        </Link>
        <Tabs
          defaultIndex={router.query.t ? tabs.findIndex((t) => t.value === router.query.t) : null}
          onChange={handleTabsChange}
        >
          <TabListStyle>
            {tabs.map((item, key) => (
              <NavTab key={key}>{t(item.transaltionId)}</NavTab>
            ))}
          </TabListStyle>
          <TabPanels tw="justify-center">
            {/* Basic infos */}
            <TabPanel>
              <SpaceForm
                mode="edit"
                space={space}
                handleChange={handleChange}
                handleChangeShowTabs={handleChangeShowTabs}
                handleSubmit={handleSubmit}
                hasUpdated={hasUpdated}
                sending={sending}
              />
            </TabPanel>
            {/* Home tab infos */}
            {/* <TabPanel>
              <SpaceForm
                mode="edit_home"
                space={space}
                handleChange={handleChange}
                handleChangeFeatured={handleChangeFeatured}
                handleSubmit={handleSubmit}
                hasUpdated={hasUpdated}
                sending={sending}
              />
            </TabPanel> */}
            {/* About tab infos */}
            <TabPanel>
              <SpaceForm
                mode="edit_about"
                space={space}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                hasUpdated={hasUpdated}
                sending={sending}
              />
              {/* waiting to have boards support for spaces */}
              <hr tw="mt-8" />
              <ManageBoards itemType="spaces" itemId={space.id} />
            </TabPanel>
            {/* Members */}
            <TabPanel>
              {space.id && <MembersList itemType="spaces" itemId={space.id} isOwner={space.is_owner} />}
            </TabPanel>
            {/* Affiliations */}
            <TabPanel>
              {/* Challenges */}
              <H2>{customChalName || t('general.challenges')}</H2>
              <div tw="my-4 justify-end">
                <Button
                  // eslint-disable-next-line react/jsx-no-bind
                  onClick={() => {
                    showModal({
                      children: (
                        <ChallengeCreate
                          isModal
                          objectId={space.id}
                          objectType="space"
                          customChalName={space.custom_challenge_name}
                          closeModal={closeModal}
                          callBack={challengesRevalidate}
                        />
                      ),
                      title: t('challenge.create.title', { challenge_wording: customChalNameSing }),
                      maxWidth: '50rem',
                      allowOverflow: true,
                    });
                  }}
                  btnType="secondary"
                >
                  {t('challenge.create.title', { challenge_wording: customChalNameSing })}
                </Button>
              </div>
              <div className="projectsAttachedList">
                <Box pb={6}>
                  <div className="justify-content-end projectsAttachedListBar">
                    <Button
                      onClick={() => {
                        showModal({
                          children: (
                            <ChallengeLinkToSpace
                              alreadyPresentChallenges={challengesData?.challenges}
                              spaceId={space.id}
                              mutateChallenges={mutateChallenges}
                              closeModal={closeModal}
                            />
                          ),
                          title: t('attach.challenge.button.text', { challenge_wording: customChalNameSing }),
                          maxWidth: '50rem',
                        });
                      }}
                    >
                      {t('attach.challenge.button.text', { challenge_wording: customChalNameSing })}
                    </Button>
                  </div>
                </Box>
                {challengesData ? (
                  <Grid gridCols={1} display={['grid', 'inline-grid']} pt={4}>
                    {challengesData?.challenges.map((challenge, i) => (
                      <ChallengeAdminCard
                        challenge={challenge}
                        key={i}
                        parentType="spaces"
                        affiliationId={challenge.affiliated_spaces[0].id}
                        callBack={challengesRevalidate}
                      />
                    ))}
                  </Grid>
                ) : (
                  <Loading />
                )}
              </div>
              {/* Projects */}
              <H2 py={3}>{t('user.profile.tab.projects')}</H2>
              <div className="projectsAttachedList">
                <Box pb={6}>
                  <div className="justify-content-end projectsAttachedListBar">
                    <Button
                      onClick={() => {
                        showModal({
                          children: (
                            <ProjectLinkToSpace
                              alreadyPresentProjects={projectsData?.projects}
                              spaceId={space.id}
                              mutateProjects={mutateProjects}
                              closeModal={closeModal}
                            />
                          ),
                          title: t('attach.project.title'),
                          maxWidth: '50rem',
                        });
                      }}
                    >
                      {t('attach.project.title')}
                    </Button>
                  </div>
                </Box>
                {projectsData ? (
                  <Grid gridCols={1} display={['grid', 'inline-grid']} pt={4}>
                    {projectsData?.projects.map((project, i) => (
                      <ProjectAdminCard
                        key={i}
                        project={project}
                        affiliationId={project.affiliated_spaces[0].id}
                        parentType="spaces"
                        callBack={projectsRevalidate}
                      />
                    ))}
                  </Grid>
                ) : (
                  <Loading />
                )}
              </div>
            </TabPanel>
            {/* FAQs */}
            <TabPanel>
              <ManageFaq itemType="spaces" itemId={space.id} />
            </TabPanel>
            {/* Partners */}
            <TabPanel>
              <SpaceForm
                mode="edit_partners"
                space={space}
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                hasUpdated={hasUpdated}
                sending={sending}
              />
            </TabPanel>
            {/* Resources */}
            <TabPanel>
              <H2 pb={3}>{t('entity.tab.documents')}</H2>
              <DocumentsManager
                documents={space.documents}
                isAdmin={space.is_admin}
                itemId={space.id}
                itemType="spaces"
              />
              <Box pb={5}></Box>
              <ManageResources itemType="spaces" itemId={space.id} />
            </TabPanel>
            {/* Advanced */}
            <TabPanel>
              <ManageExternalLink itemType="spaces" itemId={space.id} />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </div>
    </Layout>
  );
};

export async function getServerSideProps({ query, ...ctx }) {
  const api = getApiFromCtx(ctx);
  const getIdRes = await api
    .get(`/api/spaces/getid/${query.short_title}`)
    .catch((err) => console.error(`Couldn't fetch space with short_title=${query.short_title}`, err));

  if (getIdRes?.data?.id) {
    const spaceRes = await api
      .get(`/api/spaces/${getIdRes.data.id}`)
      .catch((err) => console.error(`Couldn't fetch space with id=${getIdRes.data.id}`, err));
    // Check if it got the space and if the user is admin
    if (spaceRes?.data?.is_admin) return { props: { space: spaceRes.data } };
  }
  // else redirect to homepage
  return { redirect: { destination: '/', permanent: false } };
}

export default SpaceEdit;
