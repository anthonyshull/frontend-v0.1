import $ from 'jquery';
import Link from 'next/link';
import { useRouter } from 'next/router';
import useTranslation from 'next-translate/useTranslation';
import { NextPage } from 'next';
import { ReactNode, useCallback, useEffect, useState } from 'react';
import Alert from '~/components/Tools/Alert';
import Layout from '~/components/Layout';
import { useApi } from '~/contexts/apiContext';
import Box from '~/components/Box';
import useUser from '~/hooks/useUser';
// import Image from 'next/image';
import Image2 from '~/components/Image2';
// import "./Auth.scss";
import { logEventToGA } from '~/utils/analytics';
import SpinLoader from '~/components/Tools/SpinLoader';

const Signin: NextPage = () => {
  const router = useRouter();
  const userContext = useUser();
  const api = useApi();
  const { t } = useTranslation('common');
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [errorMessage, setErrorMessage] = useState<string | ReactNode>('');
  const redirectUrl = (router.query.redirectUrl as string) ? (router.query.redirectUrl as string) : '/';

  const handleChange = (event) => {
    switch (event.target.name) {
      case 'email':
        setEmail(event.target.value);
        break;
      case 'password':
        setPassword(event.target.value);
        break;
      default:
        break;
    }
  };
  const resendVerifEmail = useCallback(() => {
    const param = {
      user: { email },
    };
    api.post('/api/users/resend_confirmation', param).then(() => {
      $('.mail-sent.alert').show(); // display confirmation message
      setTimeout(() => {
        $('.mail-sent.alert').hide(700);
      }, 2400);
    });
  }, [api, email]);

  const handleSubmit = (event) => {
    event.preventDefault();
    setLoading(true);
    userContext
      .signIn(email, password)
      .then((user) => {
        // send event to google analytics
        logEventToGA('login', 'User', `[${user.id}]`, { userId: user?.id, method: 'Website' });
        // if user doesn't have SDG or Skills, country or short bio, redirect him to complete-profile page
        if (
          user?.skills?.length === 0 ||
          user?.interests?.length === 0 ||
          user?.short_bio === null ||
          user?.country === null
        ) {
          router.push('/complete-profile');
        } else {
          // else redirect him to the page he was before signin, or the default
          router.push(redirectUrl);
        }
      })
      .catch((errors) => {
        if (errors) {
          const errorMsg = errors?.response?.data?.errors[0];
          console.warn(errorMsg);
          setError(errorMsg ? errorMsg : '');
          setLoading(false);
        }
      });
  };
  useEffect(() => {
    if (error.includes('Invalid login')) {
      // if error is about invalid login, show this message (translated)
      setErrorMessage(t('err-4010'));
    } else if (error.includes('A confirmation email was sent')) {
      // if error is that account has not been validated, show this message (translated) + button to resend mail
      setErrorMessage(
        <>
          <p>{t('signIn.resendConf')}</p>
          <button className="btn btn-primary" onClick={resendVerifEmail} type="button">
            {t('signIn.resendConf_btn')}
          </button>
          <div className="mail-sent alert alert-success" role="alert">
            {t('signIn.resendConf_msgSent')}
          </div>
        </>
      );
    } else {
      // else simply display error
      setErrorMessage(error);
    }
  }, [error, resendVerifEmail]);

  useEffect(() => {
    if (userContext.isConnected) {
      router.push('/');
    }
  });

  return (
    <Layout className="no-margin" title={`${t('header.signIn')} | JOGL`}>
      <Box pb={[10, undefined, undefined, 0]}>
        <div className="auth-form row align-items-center signIn">
          <div className="col-12 col-lg-5 leftPannel d-flex align-items-center justify-content-center">
            <Link href="/">
              <a>
                <Image2 src="/images/jogl-logo.png" className="logo" alt="JOGL icon" />
              </a>
            </Link>
          </div>
          <div className="col-12 col-lg-7 rightPannel">
            {router.query?.account_confirmation_success === 'true' && (
              <div className="alert alert-success" role="alert">
                <h4 className="alert-heading">{t('newJogler.title')}</h4>
                <p>{t('newJogler.message')}</p>
              </div>
            )}

            <div className="form-content">
              <div className="form-header">
                <h2 className="form-title" id="signModalLabel">
                  {t('signIn.title')}
                </h2>
                <p>{t('signIn.description')}</p>
              </div>
              <div className="form-body">
                <form>
                  <div className="form-group">
                    <label className="form-check-label" htmlFor="email">
                      {t('auth.email.title')}
                    </label>
                    <input
                      type="email"
                      name="email"
                      id="email"
                      className="form-control"
                      placeholder={t('auth.email.placeholder')}
                      onChange={handleChange}
                    />
                  </div>
                  <div className="form-group">
                    <div className="row rowPwd">
                      <div className="col-5">
                        <label className="form-check-label" htmlFor="password">
                          {t('signIn.pwd')}
                        </label>
                      </div>
                      <div className="col-7 text-right forgotPwd">
                        <Link href="/auth/forgot-password">
                          <a tabIndex="-1">{t('signIn.forgotPwd')}</a>
                        </Link>
                      </div>
                    </div>
                    <input
                      type="password"
                      name="password"
                      id="password"
                      className="form-control"
                      placeholder={t('signIn.pwd_placeholder')}
                      onChange={handleChange}
                    />
                  </div>
                  {error !== '' && <Alert type="danger" message={errorMessage} />}

                  <div className="goToSignUp">
                    <span>{t('signIn.newJoin')}</span>
                    <span className="goToSignUp--signup">
                      <Link href="/signup">
                        <a className="nav-link">{t('header.signUp')}</a>
                      </Link>
                    </span>
                  </div>

                  <button
                    type="submit"
                    className="btn btn-primary btn-block"
                    color="primary"
                    disabled={loading}
                    data-testid="signin-button"
                    onClick={handleSubmit}
                  >
                    {loading && <SpinLoader />}
                    {t('signIn.btnSignIn')}
                  </button>
                  {/* <div className="form-group form-check remember">
                    <input type="checkbox" className="form-check-input" id="rememberMe" />
                    <label className="form-check-label" htmlFor="rememberMe">
                      {t("signIn.remember")}
                    </label>
                  </div> */}
                </form>
              </div>
            </div>
          </div>
        </div>
      </Box>
    </Layout>
  );
};

export default Signin;
