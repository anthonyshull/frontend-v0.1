// TODO: Refactor to functional component.
import { useEffect, useState } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { useRouter } from 'next/router';
import Layout from '~/components/Layout';
import Loading from '~/components/Tools/Loading';
import FormDefaultComponent from '~/components/Tools/Forms/FormDefaultComponent';
import FormSkillsComponent from '~/components/Tools/Forms/FormSkillsComponent';
import FormInterestsComponent from '~/components/Tools/Forms/FormInterestsComponent';
import FormDropdownComponent from '~/components/Tools/Forms/FormDropdownComponent';
import Select, { createFilter } from 'react-select';
import { FixedSizeList as List } from 'react-window';
import UserCard from '~/components/User/UserCard';
import Box from '~/components/Box';
import { useApi } from '~/contexts/apiContext';
import useUserData from '~/hooks/useUserData';
import { getCountriesAndCitiesList } from '~/utils/utils';
import SpinLoader from '~/components/Tools/SpinLoader';
import { logEventToGA } from '~/utils/analytics';

// import "./CompleteProfile.scss";
const CompleteProfile = () => {
  const [sending, setSending] = useState(false);
  const [suggestedUser, setSuggestedUser] = useState(undefined);
  const [user, setUser] = useState(undefined);
  const { t } = useTranslation('common');
  const api = useApi();
  const router = useRouter();
  const { userData } = useUserData();
  const suggestedUserId = process.env.NODE_ENV === 'production' ? 191 : 1;
  // if env is dev, suggestuserId is 1, else if it's prod, make it 191 which is the JOGL user on app.jogl.io

  const [countriesCitiesData, setCountriesCitiesData] = useState([]);
  const [countryNotInList, setCountryNotInList] = useState(false);
  const [countriesList, setCountriesList] = useState([]);
  const [citiesList, setCitiesList] = useState([]);

  useEffect(() => {
    api
      .get(`/api/users/${suggestedUserId}`) // get jogl user
      .then((res) => setSuggestedUser(res.data))
      .catch((err) => console.error(`Couldn't GET user id=${suggestedUserId}`, err));
  }, []);

  useEffect(() => {
    setUser(userData);
  }, [userData]);

  const handleChange = (key, content) => {
    var newKey,
      newContent = '';
    if (content.name === 'category') {
      newKey = content.name;
      newContent = key.value;
    } else if (content.action === 'set-value' || content.action === 'select-option') {
      newKey = ['country'];
      newContent = key.value;
    } else if (content.action === 'clear') {
      newKey = ['country'];
      newContent = '';
    } else {
      newKey = key;
      newContent = content;
    }
    setUser((prevUser) => ({ ...prevUser, [newKey]: newContent }));
  };

  const handleSubmit = (event) => {
    if (event) {
      event.preventDefault();
    }
    setSending(true);
    api
      .patch(`/api/users/${user.id}`, { user })
      .then(() => {
        // send event to google analytics
        logEventToGA('completed profile', 'User', `[${user.id}]`, { userId: user.id });
        router.push(`/user/${user.id}/edit`); // then go to user edit page
      })
      .catch((err) => {
        setSending(false);
        console.error(`Couldn't PATCH user id=${user.id}`, err);
      });
  };

  getCountriesAndCitiesList(
    countriesCitiesData,
    setCountriesCitiesData,
    countryNotInList,
    setCountryNotInList,
    setCountriesList,
    setCitiesList,
    user?.country
  );

  const MenuList = ({ children, maxHeight }) => {
    return (
      <List height={maxHeight} itemCount={children.length} itemSize={30}>
        {({ index, style }) => <div style={style}>{children[index]}</div>}
      </List>
    );
  };

  const countriesSelectStyles = {
    option: (provided) => ({
      ...provided,
      padding: '4px 12px',
      cursor: 'pointer',
    }),
  };

  const disabledBtns =
    user?.interests.length === 0 ||
    !user?.short_bio ||
    !user?.category ||
    !user?.affiliation ||
    user?.skills.length === 0 ||
    !user?.country;

  return (
    <Layout noHeaderFooter title={`${t('completeProfile.title')} | JOGL`} noIndex>
      <div className="container-fluid CompleteProfile">
        <div className="align-items-center content">
          <div className="form-content">
            <div className="form-header text-center">
              <h2 className="form-title" id="signModalLabel">
                {t('completeProfile.title')}
              </h2>
              <p>{t('completeProfile.description')}</p>
            </div>
            <div className="form-body">
              <Loading active={!user} height="200px">
                <form onSubmit={handleSubmit}>
                  <FormDefaultComponent
                    id="short_bio"
                    title={t('user.profile.bioShort')}
                    placeholder={t('user.profile.bioShort_placeholder')}
                    content={user?.short_bio}
                    onChange={handleChange}
                  />
                  <FormDropdownComponent
                    id="category"
                    title={t('user.profile.category')}
                    content={user?.category}
                    // prettier-ignore
                    options={['fulltime_worker', "parttime_worker", "fulltime_student", "parttime_student",
                  "freelance", "between_jobs", "retired", "student_looking_internship"]}
                    onChange={handleChange}
                  />
                  <FormDefaultComponent
                    id="affiliation"
                    title={t('user.profile.affiliation')}
                    placeholder={t('user.profile.affiliation_placeholder')}
                    content={user?.affiliation}
                    onChange={handleChange}
                  />
                  <Select
                    name="country"
                    id="country"
                    defaultValue={user?.country && { label: user?.country, value: user?.country }}
                    options={countriesList}
                    placeholder={t('general.country_placeholder')}
                    filterOption={createFilter({ ignoreAccents: false })} // this line greatly improves performance
                    components={{ MenuList }}
                    noOptionsMessage={() => null}
                    onChange={handleChange}
                    // onChange={handleCountryChange}
                    styles={countriesSelectStyles}
                    isClearable
                  />
                  <FormSkillsComponent
                    id="skills"
                    type="user"
                    title={t('user.profile.skills')}
                    placeholder={t('general.skills.placeholder')}
                    content={user?.skills}
                    onChange={handleChange}
                  />
                  <p>{t('completeProfile.description2')}</p>
                  <FormInterestsComponent
                    title={t('user.profile.interests')}
                    content={user?.interests}
                    onChange={handleChange}
                  />
                  <Box width={['100%', '30rem']} margin="auto" textAlign="center" mt={3} mb={8}>
                    <p>{t('completeProfile.suggestFollow')}</p>
                    {suggestedUser && (
                      <UserCard
                        id={suggestedUser.id}
                        firstName={suggestedUser.first_name}
                        lastName={suggestedUser.last_name}
                        nickName={suggestedUser.nickname}
                        shortBio={suggestedUser.short_bio}
                        logoUrl={suggestedUser.logo_url}
                        hasFollowed={suggestedUser.has_followed}
                        canContact={false}
                        noMobileBorder={false}
                      />
                    )}
                  </Box>
                  <div className="buttonsContainer">
                    <button type="submit" className="btn btn-primary next" disabled={disabledBtns || sending}>
                      {sending && <SpinLoader />}
                      {t('entity.form.btnNext')}
                    </button>
                  </div>
                </form>
              </Loading>
              {!user && <p tw="text-center italic">{t('signUp.still_loading')}</p>}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};
export default CompleteProfile;
