import useTranslation from 'next-translate/useTranslation';

export const TextWithPlural = ({ type, count = 0 }) => {
  const translatedCount = count === 0 ? 1 : count; // if count is zero, make it 1, cause we have same translation if count is 0 or 1 (and so we don't need to add same translation twice)
  const { t } = useTranslation('common');
  return <>{t(`general.${type}`, { count: translatedCount })}</>;
};
